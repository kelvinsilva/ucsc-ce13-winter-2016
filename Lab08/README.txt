Kelvin Silva

Summary:

Implement morse code translator.

Important aspects:

pointers and malloc
timers
finite state machines
binary trees
recursion

Approach to lab:

Read documentation and lab manual.
Sketch out program structure, event driven programming.
implement and test

1. implement tree.c
2. implement morse decoder
3. implement state machine in morse check
4. implement string builder for morse and characters
5. implement event handler in main()

state machine was incorrect in lab manual with typos

lab manual had poor explanation of program function



Implementing lab:

It ended up meeting all specifications with no problem.

I liked that we got to work on a cool project and learn new things.

Lab manual had typos. State machine diagram in lab manual had typos and errors.
Needed explanation of the need to maintain state machine state in between function calls in timer.

Hardest part of lab was implementing morse check events

Lab was not easy to understand.
