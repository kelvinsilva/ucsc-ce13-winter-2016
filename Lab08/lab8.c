// **** Include libraries here ****
// Standard C libraries


//CMPE13 Support Library
#include "BOARD.h"

// Microchip libraries
#include <xc.h>
#include <plib.h>

// User libraries
#include "Morse.h"
#include "Tree.h"
#include "Oled.h"
#include "OledDriver.h"
#include "Buttons.h"


// **** Set any macros or preprocessor directives here ****
// Specify a bit mask for setting/clearing the pin corresponding to BTN4. Should only be used when
// unit testing the Morse event checker.
#define BUTTON4_STATE_FLAG (1 << 7)

// **** Declare any data types here ****

// **** Define any module-level, global, or external variables here ****
static MorseEvent currentEvent = MORSE_EVENT_NONE;

static char morseLine[128] = {};
static char characterLine[128] = {};

static char combinedLine[512] = {};

// **** Declare any function prototypes here ****

static void morseLineAppend(char symbol);
static void morseLineClear();
static void charLineUpdate(char symbol);


int main()
{
    BOARD_Init();


    // Configure Timer 2 using PBCLK as input. We configure it using a 1:16 prescalar, so each timer
    // tick is actually at F_PB / 16 Hz, so setting PR2 to F_PB / 16 / 100 yields a .01s timer.
    OpenTimer2(T2_ON | T2_SOURCE_INT | T2_PS_1_16, BOARD_GetPBClock() / 16 / 100);

    // Set up the timer interrupt with a medium priority of 4.
    INTClearFlag(INT_T2);
    INTSetVectorPriority(INT_TIMER_2_VECTOR, INT_PRIORITY_LEVEL_4);
    INTSetVectorSubPriority(INT_TIMER_2_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    INTEnable(INT_T2, INT_ENABLED);

/******************************************************************************
 * Your code goes in between this comment and the following one with asterisks.
 *****************************************************************************/

    OledInit();
    if (MorseInit() == STANDARD_ERROR){

        FATAL_ERROR();
    }

    
    while(1){


        switch(currentEvent){   //process morse events kelvin silva

            case MORSE_EVENT_NONE:
            {

            
            }
            break;

            case MORSE_EVENT_DOT:
            {

                char tempChar = MorseDecode(MORSE_CHAR_DOT);

                if (tempChar == SUCCESS){

                    morseLineAppend('.');
                }else {

                    
                    charLineUpdate('#');
                    morseLineClear();
                    MorseDecode(MORSE_CHAR_DECODE_RESET);
                }
            }
            break;

            case MORSE_EVENT_DASH:
            {

                char tempChar = MorseDecode(MORSE_CHAR_DASH);
                
                if (tempChar == SUCCESS){

                    morseLineAppend('-');
                }else {

                    charLineUpdate('#');
                    morseLineClear();
                    MorseDecode(MORSE_CHAR_DECODE_RESET);
                }
            }
            break;

            case MORSE_EVENT_INTER_LETTER:
            {

                char tempChar = MorseDecode(MORSE_CHAR_END_OF_CHAR);
                MorseDecode(MORSE_CHAR_DECODE_RESET);

                if (tempChar != 0){

                    morseLineClear();
                    charLineUpdate(tempChar);
                }else {

                    charLineUpdate('#');
                    morseLineClear();
                    MorseDecode(MORSE_CHAR_DECODE_RESET);                   
                }
            }
            break;

            case MORSE_EVENT_INTER_WORD:
            {

                char tempChar = MorseDecode(MORSE_CHAR_DECODE_RESET);
                if (tempChar == SUCCESS){

                    charLineUpdate(' ');
                    morseLineClear();
                }
            }

        }

        currentEvent = MORSE_EVENT_NONE; //make sure to clear event flag.

        char tempNewLineChar[2] = {'\n', 0};

        strncat(combinedLine, morseLine, 512);
        strncat(combinedLine, tempNewLineChar, 512);
        strncat(combinedLine, characterLine, 512);
        OledClear(OLED_COLOR_BLACK);
        OledDrawString(combinedLine);
        OledUpdate();

        memset(combinedLine, 0, 512);
    }

    //No program termination is programmed. memory from malloc not deallocated.

/******************************************************************************
 * Your code goes in between this comment and the preceding one with asterisks.
 *****************************************************************************/

    while (1);
}

void __ISR(_TIMER_2_VECTOR, IPL4AUTO) TimerInterrupt100Hz(void)
{
    // Clear the interrupt flag.
    IFS0CLR = 1 << 8;

    //******** Put your code here *************//
    currentEvent = MorseCheckEvents();
}

static void morseLineAppend(char symbol){

//OledClear(OLED_COLOR_BLACK);
//OledDrawString("");
//OledUpdate();

    char tempLine[2] = {0, 0};
    tempLine[0] = symbol;
    tempLine[1] = '\0';

    strncat(morseLine, tempLine, 128);


}
static void morseLineClear(){

    memset(morseLine, 0, 128);
}

static void charLineUpdate(char symbol){

    char tempLine[2] = {0, 0};
    tempLine[0] = symbol;
    tempLine[1] = '\0';

    strncat(characterLine, tempLine, 128);
}

