#include "Player.h"

static uint8_t inventory[INVENTORY_SIZE] = {};
static int inventoryIndex = 0;

/**
 * Adds the specified item to the player's inventory if the inventory isn't full.
 * @param item The item number to be stored: valid values are 0-255.
 * @return SUCCESS if the item was added, STANDARD_ERRROR if the item couldn't be added.
 */
int AddToInventory(uint8_t item){

	int errorCode = SUCCESS;

	if (inventoryIndex < INVENTORY_SIZE){

		inventory[inventoryIndex] = item;
		inventoryIndex++;
		errorCode = SUCCESS;
	}else {

		errorCode = STANDARD_ERROR;
	}

	return errorCode;
}

/**
 * Check if the given item exists in the player's inventory.
 * @param item The number of the item to be searched for: valid values are 0-255.
 * @return SUCCESS if it was found or STANDARD_ERROR if it wasn't.
 */
int FindInInventory(uint8_t item){

	int found = STANDARD_ERROR;
	int i = 0;
	for (i = 0; i < INVENTORY_SIZE; i++){

		if (item == inventory[i]){

			found = SUCCESS;
		}
	}

	return found;
}