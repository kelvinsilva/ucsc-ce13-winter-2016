// **** Include libraries here ****
// Standard libraries
#include <string.h>
#include <math.h>
#include <stdio.h>

//CMPE13 Support Library
#include "UnixBoard.h"



// User libraries
#include "Game.h"

// **** Set any macros or preprocessor directives here ****

// **** Declare any data types here ****

// **** Define any global or external variables here ****

// **** Declare any function prototypes here ****
int main()
{




    /******************************** Your custom code goes below here ********************************/
    char title[128] = {};
	char description[4096] = {};
	uint8_t dirMask = 0;
	char directionWanted = 0;

    GameInit();
    
    
    while(1){
        
        memset(title, 0, 128);
        memset(description, 0, 4096);
        directionWanted = 0;
        dirMask = 0;
        
        if (GameGetCurrentRoomTitle(title) == 0){
    
        	return 1; //error
        }
        if (GameGetCurrentRoomDescription(description) == 0){
    
        	return 1; //error
        }
    
        dirMask = GameGetCurrentRoomExits();
    
        printf( "%c[2J", 27 );
        
        printf("\e[H\e[31m\e[42m%s\e[49m\e[39m", title);
        printf("\n%s\n", description);
        
        if ( (dirMask & GAME_ROOM_EXIT_NORTH_EXISTS) == GAME_ROOM_EXIT_NORTH_EXISTS){
            
            printf("\e[4C\e[32mN\e[39m");   //print green if exists
        }else {
            
            printf("\e[4C\e[31mN\e[39m");   //printf red if not exist
        }
    
        if ( (dirMask & GAME_ROOM_EXIT_WEST_EXISTS) == GAME_ROOM_EXIT_WEST_EXISTS){
            
            printf("\n\e[32mW\e[39m");   //print green if exists
        }else {
            
            printf("\n\e[31mW\e[39m");   //printf red if not exist
        }
        
        if ( (dirMask & GAME_ROOM_EXIT_EAST_EXISTS) == GAME_ROOM_EXIT_EAST_EXISTS){
            
            printf("\e[7C\e[32mE\e[39m");   //print green if exists
        }else {
            
            printf("\e[7C\e[31mE\e[39m");   //printf red if not exist
        }
        
        if ( (dirMask & GAME_ROOM_EXIT_SOUTH_EXISTS) == GAME_ROOM_EXIT_SOUTH_EXISTS){
            
            printf("\n\e[4C\e[32mS\e[39m");   //print green if exists
        }else {
            
            printf("\n\e[4C\e[31mS\e[39m");   //printf red if not exist
        }
        
        printf("\n");
        directionWanted = getchar();
        fseek(stdin, 0, SEEK_END);
        
        
        //printf("You printed %c", directionWanted);
        switch(directionWanted){
            
            case 'N':
            case 'n':
            {
                if ( (dirMask & GAME_ROOM_EXIT_NORTH_EXISTS) == GAME_ROOM_EXIT_NORTH_EXISTS){
                    
                    GameGoNorth();
                }
            }
            break;
            
            case 'E':
            case 'e':
            {
                if ( (dirMask & GAME_ROOM_EXIT_EAST_EXISTS) == GAME_ROOM_EXIT_EAST_EXISTS){  
                    
                    GameGoEast();
                }
                
            }
            break;
            
            case 'S':
            case 's':
            {
                if ( (dirMask & GAME_ROOM_EXIT_SOUTH_EXISTS) == GAME_ROOM_EXIT_SOUTH_EXISTS){ 
                    
                    GameGoSouth();
                }
                
            }
            break;
            
            case 'W':
            case 'w':
            {
                
                if ( (dirMask & GAME_ROOM_EXIT_WEST_EXISTS) == GAME_ROOM_EXIT_WEST_EXISTS){ 
                    
                    GameGoWest();
                }
            }
            break;
            
            default :
            {
                
                //nothing, reprint room.
            }
        }
        
    }
    
    return 0;
    /**************************************************************************************************/
}


