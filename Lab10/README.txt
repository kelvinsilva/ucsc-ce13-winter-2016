Kelvin Silva

Summary:

To implement a text based game program that uses operating system resources, file i/o

Important aspects:

unix, file i/o

Approach to lab:

Read documentation and lab manual.
Sketch out program structure, read about file i/o
implement and test

Not many problems occurred.

Took effort to read and understand vt-100 terminal commands

Implementing lab:

It ended up meeting all specifications with no problem.

I liked that we got to work on a cool project and learn new things.
It was fun learning file i/o

Lab manual was not that bad. needed more description of user interaction with program.
example video would be helpful.


Hardest part of lab was debouncing and reading lab manual for project requirements.
