#include "Game.h"
#include "Player.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static char currentRoomString[128] = {};
static uint8_t file[8192] = {};
static unsigned int sizeOfFile = 0;

typedef struct {

	char title[128];
	char description[4096];

	uint8_t exitsMask;

	uint8_t roomNumberExits[4];

	int numberOfItemRequirements;
	int numberOfItemsContained;

	uint8_t itemRequirements[512];
	uint8_t itemsContained[512];

}RoomFileContainer;

static int roomVersionToDisplay = 0;
static int numberOfRoomVersionsTotal = 0;
static RoomFileContainer roomContainers[8]; //hold multiple versions of a room


static void cleanCurrentRoomFromMemory();	//clear memory that holds game data. structures and file memory location all set to 0
static void buildGameRoomExitFlagsMaskFromBytes(RoomFileContainer* rfcPtr); //make mask for nsew directions
static void memoryToRoomContainer(uint8_t file[]);	//parses binary data and inputs into structure
static int roomFileToMemory(char roomFilePath[]);	//copies file (on secondary memory) to memory

static void buildRoomFileNameWithInteger(int roomNumber);
static void gameDetermineCorrectRoomVersion();	//checks player inventory and determines which structure to use for display
static int gameAddItemsToPlayerInventory();	//adds items from room into player inventory -- called in main

//returns 1 if inventory is full. 0 if inventory was successfully filled or there is still more space in inventory
static int gameAddItemsToPlayerInventory(){

	int inventoryFillSuccess = 0;

	int i = 0;
	for (i = 0; i < roomContainers[roomVersionToDisplay].numberOfItemsContained; i++){

		if (AddToInventory(roomContainers[roomVersionToDisplay].itemsContained[i]) == STANDARD_ERROR){

			inventoryFillSuccess = 1;	//inventory is full.
			break;
		}
	}

	return inventoryFillSuccess;
}

static void gameDetermineCorrectRoomVersion(){

	int saveValue = 0;

	int i = 0;
	for (i = 0; i < numberOfRoomVersionsTotal; i++){

		int j = 0;
		for (j = 0; j < roomContainers[i].numberOfItemRequirements; j++){

			if (FindInInventory(roomContainers[i].itemRequirements[j]) == SUCCESS){

				saveValue++;
			}
		}
		if (saveValue == roomContainers[i].numberOfItemRequirements){

			break;
		}else {


		}
	}

	roomVersionToDisplay = i;
}

static void buildRoomFileNameWithInteger(int roomNumber){


	char currentRoom[128] = "./RoomFiles/room";
	char extension[128] = ".txt";
	char roomNumberStr[64];
	sprintf(roomNumberStr, "%d", roomNumber);

	strcat(currentRoom, roomNumberStr);
	strcat(currentRoom, extension);

	memcpy(currentRoomString, currentRoom, strlen(currentRoom) );
}

//loads the file into memory into a string called file
//returns zero for success, non zero for failure
static int roomFileToMemory(char roomFilePath[]){

	FILE *filePtr = NULL;
	filePtr = fopen(roomFilePath, "rb");

	if (filePtr == NULL){

		printf("File error, cannot open, FILEPATH: %s", roomFilePath);
		return 1;
	}

	sizeOfFile = fread(file, sizeof(uint8_t), 8192, filePtr);	//module level variable assignment

	fclose(filePtr);
	return 0;
}

static void memoryToRoomContainer(uint8_t file[]){

	int index = 0;		//holds where in memory we are at
	int roomContainerIndex = 0;	//stores which version of room we are at.
	char title[GAME_MAX_ROOM_TITLE_LENGTH] = {};	//need to store copy of title in case there are more versions.


	//step 1, read the amount of bytes -- length of title
	int sizeOfTitle = file[index];
	index++;	//move index to start of title.

	int i = 0;
	for (i = 0; i < sizeOfTitle; i++){

		title[i] = file[index];
		index++;
	}
	//at end of loop, index is already at beginning of item requirements size.

	//iterate over this for amount of versions contained in room;
	int endOfFile = 0;
	while(endOfFile == 0){

		//title has been loaded into memory. now need to fill out field of structure.
		memcpy( roomContainers[roomContainerIndex].title, title, sizeOfTitle);

		//this portion will be iterated over the amount of room containers, i.e. versions of the room that exists.

		//step 2 read item requirements
		int numberOfItemRequirements = file[index];	//index is at item requirements
		roomContainers[roomContainerIndex].numberOfItemRequirements = numberOfItemRequirements;
		index++;								//move index to beginning of items to be read;

		//remember that variable i is in scope at line 82
		for (i = 0; i < numberOfItemRequirements; i++){

			roomContainers[roomContainerIndex].itemRequirements[i] = file[index];
			index++;
		}

		//now index is at beginning of description size
		int sizeOfDescription = file[index];
		index++; //move index to beginning of description

		for (i = 0; i < sizeOfDescription; i++){

			roomContainers[roomContainerIndex].description[i] = file[index];
			index++;
		}
		//now index is at beginning of size of items contained
		int sizeOfItemsContained = file[index];
		roomContainers[roomContainerIndex].numberOfItemsContained = sizeOfItemsContained;
		index++; 		//move index to beginning of items contained to be read;

		for (i = 0; i < sizeOfItemsContained; i++){

			roomContainers[roomContainerIndex].itemsContained[i] = file[index];
			index++;
		}
		//now index is at beginning of first exit byte.

		//first exit byte is north, so assign to first index of room container structure
		roomContainers[roomContainerIndex].roomNumberExits[0] = file[index];
		index++; //index is now at byte indicating east exit.

		roomContainers[roomContainerIndex].roomNumberExits[1] = file[index]; //assign east byte
		index++; //index is now at byte indicating south exit;

		roomContainers[roomContainerIndex].roomNumberExits[2] = file[index];
		index++;	//index is now at byte indicating west exit;

		roomContainers[roomContainerIndex].roomNumberExits[3] = file[index];
		index++; //index is now at end of file OR at the beginning of next version, which is the byte indicating the size of item requirements

		buildGameRoomExitFlagsMaskFromBytes(&roomContainers[roomContainerIndex]);	//build mask from 4 bytes retrieved from file above.


		//Now must test if at end of file.
		if ( sizeOfFile == index){	//if condition is true then we are at the end of file.

			endOfFile = 1;	//set end of file flag.
		}else {
		 
						//if condition is false, then there must be another version to the file, do nothing and continue loop
			roomContainerIndex++;
		}
	}
	numberOfRoomVersionsTotal = roomContainerIndex;
	
}

//performs bitwise oring of field roomNumberExits[] in RoomFileContainer structure
static void buildGameRoomExitFlagsMaskFromBytes(RoomFileContainer* rfcPtr){

	uint8_t tempMask = 0;
	uint8_t tempBit = 0b1000;

	int i = 0;
	for (i = 0; i < 4; i++){

		if (rfcPtr->roomNumberExits[i] > 0){

            tempMask |= tempBit;
		}
        tempBit = (tempBit >> 1);
	}
	rfcPtr->exitsMask = tempMask;
}

static void cleanCurrentRoomFromMemory(){

	memset(&roomContainers, 0, sizeof(RoomFileContainer) * 8);	//set all structs to zero
	memset(currentRoomString, 0 , 128);
	memset(file, 0, 8192);
	sizeOfFile = 0;
	roomVersionToDisplay = 0;
	numberOfRoomVersionsTotal = 0;
}



/**
 * These function transitions between rooms. Each call should return SUCCESS if the current room has
 * an exit in the correct direction and the new room was able to be loaded, and STANDARD_ERROR
 * otherwise.
 * @return SUCCESS if the room CAN be navigated to and changing the current room to that new room
 *         succeeded.
 */
int GameGoNorth(void){

	int roomNumberToGoTo = roomContainers[roomVersionToDisplay].roomNumberExits[0];



	int errorCode = SUCCESS;

	if ( (roomContainers[roomVersionToDisplay].exitsMask &  GAME_ROOM_EXIT_NORTH_EXISTS) == GAME_ROOM_EXIT_NORTH_EXISTS ){ //if condition is true, means can go to room

		cleanCurrentRoomFromMemory();
		//load starting room
		memset(&roomContainers, 0, sizeof(RoomFileContainer) * 8);	//set all structs to zero
		buildRoomFileNameWithInteger(roomNumberToGoTo); //set currentRoomString module level variable to room32.txt

		if (roomFileToMemory(currentRoomString)){ //fill in file module level variable with bytes from room32.txt

			//error occurred. check return value of roomFileToMemory
			errorCode = STANDARD_ERROR;
		}

		memoryToRoomContainer(file);			//fill in structure of room containers from bytes of memory (file[] variable).
		//game data is now loaded into structure.
		gameDetermineCorrectRoomVersion();
		gameAddItemsToPlayerInventory();
	}else {

		errorCode = STANDARD_ERROR;
	}

	return errorCode;
}

/**
 * @see GameGoNorth
 */
int GameGoEast(void){

	int roomNumberToGoTo = roomContainers[roomVersionToDisplay].roomNumberExits[1];

	int errorCode = SUCCESS;

	if ( (roomContainers[roomVersionToDisplay].exitsMask &  GAME_ROOM_EXIT_EAST_EXISTS) == GAME_ROOM_EXIT_EAST_EXISTS ){ //if condition is true, means can go to room

		cleanCurrentRoomFromMemory();
		//load starting room
		memset(&roomContainers, 0, sizeof(RoomFileContainer) * 8);	//set all structs to zero
		buildRoomFileNameWithInteger(roomNumberToGoTo); //set currentRoomString module level variable to room32.txt

		if (roomFileToMemory(currentRoomString)){ //fill in file module level variable with bytes from room32.txt

			//error occurred. check return value of roomFileToMemory
			errorCode = STANDARD_ERROR;
		}

		memoryToRoomContainer(file);			//fill in structure of room containers from bytes of memory (file[] variable).
		//game data is now loaded into structure.
		gameDetermineCorrectRoomVersion();
		gameAddItemsToPlayerInventory();
	}else {

		errorCode = STANDARD_ERROR;
	}

	return errorCode;
}

/**
 * @see GameGoNorth
 */
int GameGoSouth(void){

	int roomNumberToGoTo = roomContainers[roomVersionToDisplay].roomNumberExits[2];

	int errorCode = SUCCESS;

	if ( (roomContainers[roomVersionToDisplay].exitsMask &  GAME_ROOM_EXIT_SOUTH_EXISTS) == GAME_ROOM_EXIT_SOUTH_EXISTS ){ //if condition is true, means can go to room

		cleanCurrentRoomFromMemory();
		//load starting room
		memset(&roomContainers, 0, sizeof(RoomFileContainer) * 8);	//set all structs to zero
		buildRoomFileNameWithInteger(roomNumberToGoTo); //set currentRoomString module level variable to room32.txt

		if (roomFileToMemory(currentRoomString)){ //fill in file module level variable with bytes from room32.txt

			//error occurred. check return value of roomFileToMemory
			errorCode = STANDARD_ERROR;
		}

		memoryToRoomContainer(file);			//fill in structure of room containers from bytes of memory (file[] variable).
		//game data is now loaded into structure.
		gameDetermineCorrectRoomVersion();
		gameAddItemsToPlayerInventory();
	}else {
		
		errorCode = STANDARD_ERROR;
	}

	return errorCode;
}

/**
 * @see GameGoNorth
 */
int GameGoWest(void){

	int roomNumberToGoTo = roomContainers[roomVersionToDisplay].roomNumberExits[3];

	int errorCode = SUCCESS;

	if ( (roomContainers[roomVersionToDisplay].exitsMask &  GAME_ROOM_EXIT_WEST_EXISTS) == GAME_ROOM_EXIT_WEST_EXISTS ){ //if condition is true, means can go to room

		cleanCurrentRoomFromMemory();
		//load starting room
		memset(&roomContainers, 0, sizeof(RoomFileContainer) * 8);	//set all structs to zero
		buildRoomFileNameWithInteger(roomNumberToGoTo); //set currentRoomString module level variable to room32.txt

		if (roomFileToMemory(currentRoomString)){ //fill in file module level variable with bytes from room32.txt

			//error occurred. check return value of roomFileToMemory
			errorCode = STANDARD_ERROR;
		}

		memoryToRoomContainer(file);			//fill in structure of room containers from bytes of memory (file[] variable).
		//game data is now loaded into structure.
		gameDetermineCorrectRoomVersion();
		gameAddItemsToPlayerInventory();
	}else {

		errorCode = STANDARD_ERROR;
	}

	return errorCode;
}

/**
 * This function sets up anything that needs to happen at the start of the game. This is just
 * setting the current room to STARTING_ROOM and loading it. It should return SUCCESS if it succeeds
 * and STANDARD_ERROR if it doesn't.
 * @return SUCCESS or STANDARD_ERROR
 */
int GameInit(void){

	int errorCode = SUCCESS;

	//load starting room
	
	memset(&roomContainers, 0, sizeof(RoomFileContainer) * 8);	//set all structs to zero
	buildRoomFileNameWithInteger(STARTING_ROOM); //set currentRoomString module level variable to room32.txt
	
	if (roomFileToMemory(currentRoomString)){ //fill in file module level variable with bytes from room32.txt

		//error occurred. check return value of roomFileToMemory
		errorCode = STANDARD_ERROR;
	}else {

		memoryToRoomContainer(file);			//fill in structure of room containers from bytes of memory (file[] variable).
		//game data is now loaded into structure.
		gameDetermineCorrectRoomVersion();
		gameAddItemsToPlayerInventory();
	}

	return errorCode;
}

/**
 * Copies the current room title as a NULL-terminated string into the provided character array.
 * Only a NULL-character is copied if there was an error so that the resultant output string
 * length is 0.
 * @param title A character array to copy the room title into. Should be GAME_MAX_ROOM_TITLE_LENGTH+1
 *             in length in order to allow for all possible titles to be copied into it.
 * @return The length of the string stored into `title`. Note that the actual number of chars
 *         written into `title` will be this value + 1 to account for the NULL terminating
 *         character.
 */
int GameGetCurrentRoomTitle(char *title){

	memcpy(title, roomContainers[roomVersionToDisplay].title, strlen(roomContainers[roomVersionToDisplay].title) );

	return strlen(title);
}

/**
 * GetCurrentRoomDescription() copies the description of the current room into the argument desc as
 * a C-style string with a NULL-terminating character. The room description is guaranteed to be less
 * -than-or-equal to GAME_MAX_ROOM_DESC_LENGTH characters, so the provided argument must be at least
 * GAME_MAX_ROOM_DESC_LENGTH + 1 characters long. Only a NULL-character is copied if there was an
 * error so that the resultant output string length is 0.
 * @param desc A character array to copy the room description into.
 * @return The length of the string stored into `desc`. Note that the actual number of chars
 *          written into `desc` will be this value + 1 to account for the NULL terminating
 *          character.
 */
int GameGetCurrentRoomDescription(char *desc){

	memcpy(desc, roomContainers[roomVersionToDisplay].description, strlen(roomContainers[roomVersionToDisplay].description) );

	return strlen(desc);
}

/**
 * This function returns the exits from the current room in the lowest-four bits of the returned
 * uint8 in the order of NORTH, EAST, SOUTH, and WEST such that NORTH is in the MSB and WEST is in
 * the LSB. A bit value of 1 corresponds to there being a valid exit in that direction and a bit
 * value of 0 corresponds to there being no exit in that direction. The GameRoomExitFlags enum
 * provides bit-flags for checking the return value.
 *
 * @see GameRoomExitFlags
 *
 * @return a 4-bit bitfield signifying which exits are available to this room.
 */
uint8_t GameGetCurrentRoomExits(void){

	return roomContainers[roomVersionToDisplay].exitsMask;
}
