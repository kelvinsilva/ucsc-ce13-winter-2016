// **** Include libraries here ****
// Standard libraries
#include <stdio.h>

//Class specific libraries
#include "BOARD.h"

// Microchip libraries
#include <xc.h>

// User libraries
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    BOARD_Init();
    /***************************************************************************************************
     * Your code goes in between this comment and the following one with asterisks.
     **************************************************************************************************/

    // Declare Variables
    float fahr, celsius;
    int lower, upper, step;

    //initialize variables
    lower = 0; //lower limit of temperature
    upper = 300; //upper limit
    step = 20; //step size;
    fahr = lower;
    
    //Print header, adjust 5 spaces
    printf("%*c", 5, 'F');
    printf("%*c", 5, 'C');
    printf("\n");

    //Print out table
    while (fahr <= upper) {
        
        celsius = (5.0 / 9.0)*(fahr - 32.0);
        printf("%7.1f %04.0f\n", (double) fahr, (double) celsius);  //adjust 7 padded with no character, and a 1 decimal on the right side for farenheight
                                                                    //adjust 4 padded with zeroes, no decimal on the right for celcius
        fahr = fahr + step;
    }
    printf("\n");
    
    double kelvin = 0;
    kelvin = lower;  
    
    //Print header
    printf("%*c", 5, 'K');
    printf("%*c", 5, 'F');
    printf("\n");
    
    //Print out table
    
    while (kelvin <= upper) {

        fahr = (((double)kelvin * (9.0/5.0)) - 459.67);
        //adjust 3 on the left with 0 padded, and 3 decimals on the right for kelvin
        //adjust 5 on the left for farenheight, no decimal precision
        printf("%03.3f %5f\n", (double) kelvin, (double) fahr);
        kelvin = kelvin + step;
    }
    
    /***************************************************************************************************
     * Your code goes in between this comment and the preceding one with asterisks.
     **************************************************************************************************/

    // Returning from main() is bad form in embedded environments. So we sit and spin.
    while (1);
}
