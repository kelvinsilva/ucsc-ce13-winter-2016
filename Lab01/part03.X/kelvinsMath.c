float absol(float x);   //absolute value function

float arctangent2(float y, float x){
    
    float ybar = absol(y);
    
    float r = 0;
    float phi = 0;
    
    if (x > 0){
    
        r = (x - ybar) / (x + ybar);
        phi = ( (0.1963) * (r*r*r) ) - (0.9817 * r) + (3.14159/4);
        
    }else {
        
        r = (x + ybar) / (x - ybar);
        phi = ( (0.1963) * (r*r*r) ) - (0.9817 * r) + ((3.14159*3)/4);
    }
    
    if ( y < 0 ){
        
        phi = (-1 * phi);
    }
    
    return phi;
}


float enorm(float px, float py, float qx, float qy){
    
    float dx = absol (qx - px);
    float dy = absol (qy - py);
    
    float g = 0;
    float e = 0;
    
    if (dx > dy){
        
        g = dx;
        e = dy;
    } else {
        
        g = dy;
        e = dx;
    }
    
    int i = 0;
    
    float t = 0;
    float r = 0;
    float s = 0;
    
    for (i = 0; i < 2; i++){
        
        t = (e/g);
        r = (t * t);
        
        s = (r/(4.0 + r));
        g = (g + (2 * s * g));
        e = (e * s);
    }
    
    return g;
}

float absol(float x) {
    
    return ( (x < 0) ? (x * -1) : x );  //if x is less than 0 then return x * -1, else return x, ternary operator
}

