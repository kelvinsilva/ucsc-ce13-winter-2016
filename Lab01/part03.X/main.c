/* 
 * File:   main.c
 * Author: kelvin silva
 *
 * Created on January 8, 2016, 10:24 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include <math.h>
#include "BOARD.h"
#include "kelvinsMath.h"

/*
 * 
 */
int main()
{
    
    BOARD_Init();
    int pt1x = 0;
    int pt1y = 0;
    
    int pt2x = 0;
    int pt2y = 0;
    
    int x = 0;
    int y = 0;
    
    float distance = 0;
    int i = 0;
    
    printf ("Testing standard hypot function and custom euclidean normal function: ");
    //test random values for enorm and hypot
    for (i = 0; i < 5; i++){
        
        //choose random values
        pt1x = rand() % 10;
        pt2x = rand() % 10;
        
        pt1y = rand() % 10;
        pt2y = rand() % 10;
        
        //distance between two points for x and y value
        x = abs(pt1x - pt2x);
        y = abs(pt1y - pt2y);
        
        distance = hypot(x, y);

        printf ("\nDistance between (%i, %i) and (%i, %i) with standard hypot  %f", pt1x ,pt1y ,pt2x ,pt2y ,distance);
        
        distance = enorm(pt1x, pt1y, pt2x, pt2y);

        printf ("\nDistance between (%i, %i) and (%i, %i) with custom enorm is  %f", pt1x ,pt1y ,pt2x ,pt2y , distance);
    }
    
    printf ("\nTesting standard atan2 function and custom arctangent function: ");
    
    //create new variables for next loop
    float atanswer = 0;
    int testY = 0;
    int testX = 0;
    
    //test random values for arctangent and atan2
    for (i = 0; i < 5; i++){
        
        testY = rand() % 10;
        testX = rand() % 10;
        
        atanswer = atan2(testY, testX);

        printf ("\nArctangent with standard function atan2 using x: %i, and y: %i returns %f ", testX, testY, atanswer);
        
        atanswer = arctangent2(testY, testX);

        printf ("\nArctangent with custom function arctangent2 using x: %i, and y: %i returns %f", testX, testY, atanswer);
    }    
    
  
    
    while(1);
}
