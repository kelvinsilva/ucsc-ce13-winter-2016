#ifndef KELVINSMATH_H_
#define KELVINSMATH_H_

float arctangent2(float y, float x);    //arctangent for all quadrant
float enorm(float px, float py, float qx, float qy);    //euclidean norm function, aka distance formula



#endif //KELVINSMATH_H_