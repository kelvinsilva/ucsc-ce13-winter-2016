/* 
 * File:   main.c
 * Author: kelvi
 *
 * Created on January 11, 2016, 9:15 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "BOARD.h"
#include "Ascii.h"
#include "Oled.h"
#include "OledDriver.h"




/*
 * 
 */
int main(int argc, char** argv)
{

    BOARD_Init();   //Initialize board
    
    OledInit();     //Initilize oled

    OledDrawString("Hello World");  //"Draw string to oled
    
    OledUpdate();       //Refresh to show
    
    while(1);          
}

