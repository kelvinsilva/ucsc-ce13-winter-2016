#include "Agent.h"
#include "Protocol.h"
#include "Field.h"
#include "BOARD.h"
#include "Oled.h"
#include "FieldOled.h"
#include "Leds.h"
#include <stdlib.h>


static Field myField;
static Field enemyField;
static FieldOledTurn currentTurn = FIELD_OLED_TURN_NONE;

//convert protocol parser status to agentevent
static AgentEvent protocolParserStatusToAgentEvent(ProtocolParserStatus in);

/**
 * The Init() function for an Agent sets up everything necessary for an agent before the game
 * starts. This can include things like initialization of the field, placement of the boats,
 * etc. The agent can assume that stdlib's rand() function has been seeded properly in order to
 * use it safely within.
 */
void AgentInit(void)
{
    FieldInit(&myField, FIELD_POSITION_EMPTY);
    FieldInit(&enemyField, FIELD_POSITION_UNKNOWN);
    //Randomly place small medium, large and huge boats
    while (!FieldAddBoat(&myField, rand() % 6, rand() % 10, rand() % 4, FIELD_BOAT_SMALL));
    while (!FieldAddBoat(&myField, rand() % 6, rand() % 10, rand() % 4, FIELD_BOAT_MEDIUM));
    while (!FieldAddBoat(&myField, rand() % 6, rand() % 10, rand() % 4, FIELD_BOAT_LARGE));
    while (!FieldAddBoat(&myField, rand() % 6, rand() % 10, rand() % 4, FIELD_BOAT_HUGE));
    OledClear(OLED_COLOR_BLACK);
    FieldOledDrawScreen(&myField, &enemyField, currentTurn);
    OledUpdate();
}

/**
 * The Run() function for an Agent takes in a single character. It then waits until enough
 * data is read that it can decode it as a full sentence via the Protocol interface. This data
 * is processed with any output returned via 'outBuffer', which is guaranteed to be 255
 * characters in length to allow for any valid NMEA0183 messages. The return value should be
 * the number of characters stored into 'outBuffer': so a 0 is both a perfectly valid output and
 * means a successful run.
 * @param in The next character in the incoming message stream.
 * @param outBuffer A string that should be transmit to the other agent. NULL if there is no
 *                  data.
 * @return The length of the string pointed to by outBuffer (excludes \0 character).
 */

//create empty myNegotiation and incomingNegotiation and set current state 
static NegotiationData myNegotiation = {};
static NegotiationData incomingNegotiation = {};
static AgentState currentState = AGENT_STATE_GENERATE_NEG_DATA;

int AgentRun(char in, char *outBuffer)
{

    GuessData incomingGuess = {};
    AgentEvent status = protocolParserStatusToAgentEvent(ProtocolDecode(in, &incomingNegotiation, &incomingGuess));
    memset(outBuffer, 0, 64);

    switch (currentState) {

    case AGENT_STATE_GENERATE_NEG_DATA:
    {

        //Check if parsing message failed, got to invalid state
        if (status == AGENT_EVENT_MESSAGE_PARSING_FAILED) {
            OledClear(OLED_COLOR_BLACK);
            OledDrawString(AGENT_ERROR_STRING_PARSING);
            OledUpdate();
            currentState = AGENT_STATE_INVALID;
            return 0;
        } else {
            //Generate negotiation data and send challenge data
            ProtocolGenerateNegotiationData(&myNegotiation);
            int stringLength = ProtocolEncodeChaMessage(outBuffer, &myNegotiation);
            currentState = AGENT_STATE_SEND_CHALLENGE_DATA;
            return stringLength;
        }
    }
        break;

    case AGENT_STATE_SEND_CHALLENGE_DATA:
    {
        //Check if parsing message failed, got to invalid state
        if (status == AGENT_EVENT_MESSAGE_PARSING_FAILED) {
            OledClear(OLED_COLOR_BLACK);
            OledDrawString(AGENT_ERROR_STRING_PARSING);
            OledUpdate();
            currentState = AGENT_STATE_INVALID;
            return 0;
        } else if (status == AGENT_EVENT_RECEIVED_CHA_MESSAGE) {
            //Received opponent data, send determine data
            int stringLength = ProtocolEncodeDetMessage(outBuffer, &myNegotiation);
            currentState = AGENT_STATE_DETERMINE_TURN_ORDER;
            return stringLength;
        } else {
            currentState = AGENT_STATE_SEND_CHALLENGE_DATA;
            return 0;
        }

    }
        break;

    case AGENT_STATE_DETERMINE_TURN_ORDER:
    {
        //Check if parsing message failed, got to invalid state
        if (status == AGENT_EVENT_MESSAGE_PARSING_FAILED) {
            OledClear(OLED_COLOR_BLACK);
            OledDrawString(AGENT_ERROR_STRING_PARSING);
            OledUpdate();
            currentState = AGENT_STATE_INVALID;
            return 0;
        } else if (status == AGENT_EVENT_RECEIVED_DET_MESSAGE) {
            //NegotiationData currentNegotiation = {};
            //check if det message was received
            uint8_t validationFlag = ProtocolValidateNegotiationData(&incomingNegotiation);
            if (validationFlag == SUCCESS) {
                TurnOrder winLoseOrTie = ProtocolGetTurnOrder(&myNegotiation, &incomingNegotiation);
                if (winLoseOrTie == TURN_ORDER_TIE) {
                    //incoming data tie with my data
                    currentState = AGENT_STATE_INVALID;
                    OledClear(OLED_COLOR_BLACK);
                    OledDrawString(AGENT_ERROR_STRING_ORDERING);
                    OledUpdate();
                    return 0;
                } else if (winLoseOrTie == TURN_ORDER_START) {
                    //incoming data lost, my data won
                    currentTurn = FIELD_OLED_TURN_MINE;
                    currentState = AGENT_STATE_SEND_GUESS;
                    OledClear(OLED_COLOR_BLACK);
                    FieldOledDrawScreen(&myField, &enemyField, currentTurn);
                    OledUpdate();
                    return 0;

                } else if (winLoseOrTie == TURN_ORDER_DEFER) {
                    //incoming data won, my data lost
                    currentTurn = FIELD_OLED_TURN_THEIRS;
                    currentState = AGENT_STATE_WAIT_FOR_GUESS;
                    OledClear(OLED_COLOR_BLACK);
                    FieldOledDrawScreen(&myField, &enemyField, currentTurn);
                    OledUpdate();
                    return 0;
                }
            } else {
                currentState = AGENT_STATE_INVALID;
                OledClear(OLED_COLOR_BLACK);
                OledDrawString(AGENT_ERROR_STRING_NEG_DATA);
                OledUpdate();
                return 0;
            }
        }


    }
        break;

    case AGENT_STATE_WAIT_FOR_GUESS:
    {

        //Check if parsing message failed, got to invalid state
        if (status == AGENT_EVENT_MESSAGE_PARSING_FAILED) {
            OledClear(OLED_COLOR_BLACK);
            OledDrawString(AGENT_ERROR_STRING_PARSING);
            OledUpdate();
            currentState = AGENT_STATE_INVALID;
            return 0;
        } else if ((status == AGENT_EVENT_RECEIVED_COO_MESSAGE) && ((FieldGetBoatStates(&myField) > 0))) {
            //if boats are still alive and receive coo message, set hit turn to mine

            currentTurn = FIELD_OLED_TURN_MINE;
            FieldRegisterEnemyAttack(&myField, &incomingGuess);
            OledClear(OLED_COLOR_BLACK);
            FieldOledDrawScreen(&myField, &enemyField, currentTurn);
            OledUpdate();
            //encode an incoming guess
            GuessData gTemp = {};

            gTemp.row = incomingGuess.row;
            gTemp.col = incomingGuess.col;

            gTemp.hit = incomingGuess.hit;

            int stringLength = ProtocolEncodeHitMessage(outBuffer, &gTemp);
            currentState = AGENT_STATE_SEND_GUESS;
            return stringLength;
        } else if ((status == AGENT_EVENT_RECEIVED_COO_MESSAGE) && ((FieldGetBoatStates(&myField) == 0))) {

            currentTurn = FIELD_OLED_TURN_NONE;

            //register enemy attack
            //FieldPosition boatOrNot = FieldUpdateKnowledge(&enemyField, &incomingGuess);
            FieldRegisterEnemyAttack(&myField, &incomingGuess);
            OledClear(OLED_COLOR_BLACK);
            FieldOledDrawScreen(&myField, &enemyField, currentTurn);
            OledUpdate();

            GuessData gTemp = {};

            gTemp.row = incomingGuess.row;
            gTemp.col = incomingGuess.col;

            gTemp.hit = incomingGuess.hit;

            int stringLength = ProtocolEncodeHitMessage(outBuffer, &gTemp);

            currentState = AGENT_STATE_LOST;
            return stringLength;
        } else {

            currentState = AGENT_STATE_WAIT_FOR_GUESS;
            return 0;
        }
    }
        break;

    case AGENT_STATE_SEND_GUESS:
    {

        //Check if parsing message failed, got to invalid state
        if (status == AGENT_EVENT_MESSAGE_PARSING_FAILED) {
            OledClear(OLED_COLOR_BLACK);
            OledDrawString(AGENT_ERROR_STRING_PARSING);
            OledUpdate();
            currentState = AGENT_STATE_INVALID;
            return 0;
        }
        GuessData gTemp = {};
        //generate random guess that hasn't been guessed before
        while (1) {

            gTemp.row = rand() % 6;
            gTemp.col = rand() % 10;

            FieldPosition tempPosition = FieldAt(&enemyField, gTemp.row, gTemp.col);
            if ((tempPosition != FIELD_POSITION_MISS) && (tempPosition != FIELD_POSITION_HIT)) {
                break;
            }

        }
        int i = 0;
        for (i = 0; i < (BOARD_GetPBClock() / 8); i++);

        int stringLength = ProtocolEncodeCooMessage(outBuffer, &gTemp);

        currentState = AGENT_STATE_WAIT_FOR_HIT;
        return stringLength;
    }
        break;

    case AGENT_STATE_WAIT_FOR_HIT:
    {
        //printf("\nwaiting for hit");
        //Check if parsing message failed, got to invalid state
        if (status == AGENT_EVENT_MESSAGE_PARSING_FAILED) {
            OledClear(OLED_COLOR_BLACK);
            OledDrawString(AGENT_ERROR_STRING_PARSING);
            OledUpdate();
            currentState = AGENT_STATE_INVALID;
            return 0;
        } else if (status == AGENT_EVENT_RECEIVED_HIT_MESSAGE) {
            //if enemy boats are still alive switch to wait for guess
            if (FieldGetBoatStates(&enemyField) > 0) {

                FieldUpdateKnowledge(&enemyField, &incomingGuess);
                currentTurn = FIELD_OLED_TURN_THEIRS;
                OledClear(OLED_COLOR_BLACK);
                FieldOledDrawScreen(&myField, &enemyField, currentTurn);
                OledUpdate();

                currentState = AGENT_STATE_WAIT_FOR_GUESS;
                return 0;

            } else if (FieldGetBoatStates(&enemyField) == 0) {
                //no boats are left then set to won
                currentTurn = FIELD_OLED_TURN_NONE;
                OledClear(OLED_COLOR_BLACK);
                FieldOledDrawScreen(&myField, &enemyField, currentTurn);
                OledUpdate();
                currentState = AGENT_STATE_WON;
                return 0;
            }
        } else {

            currentState = AGENT_STATE_WAIT_FOR_HIT;
            return 0;
        }
    }
        break;

    case AGENT_STATE_INVALID:
    {
        OledClear(OLED_COLOR_BLACK);
        OledDrawString(AGENT_ERROR_STRING_PARSING);
        return 0;
    }

    case AGENT_STATE_WON:
    {

    }
        break;

    case AGENT_STATE_LOST:
    {

    }
        break;

    }
    return 0;
}

/**
 * StateCheck() returns a 4-bit number indicating the status of that agent's ships. The smallest
 * ship, the 3-length one, is indicated by the 0th bit, the medium-length ship (4 tiles) is the
 * 1st bit, etc. until the 3rd bit is the biggest (6-tile) ship. This function is used within
 * main() to update the LEDs displaying each agents' ship status. This function is similar to
 * Field::FieldGetBoatStates().
 * @return A bitfield indicating the sunk/unsunk status of each ship under this agent's control.
 *
 * @see Field.h:FieldGetBoatStates()
 * @see Field.h:BoatStatus
 */
uint8_t AgentGetStatus(void)
{
    return FieldGetBoatStates(&myField);
}

/**
 * This function returns the same data as `AgentCheckState()`, but for the enemy agent.
 * @return A bitfield indicating the sunk/unsunk status of each ship under the enemy agent's
 *         control.
 *
 * @see Field.h:FieldGetBoatStates()
 * @see Field.h:BoatStatus
 */
uint8_t AgentGetEnemyStatus(void)
{
    return FieldGetBoatStates(&enemyField);
}

//convert protocol  messages into agent events

AgentEvent protocolParserStatusToAgentEvent(ProtocolParserStatus in)
{

    if (in == PROTOCOL_WAITING || (in == PROTOCOL_PARSING_GOOD)) {

        return AGENT_EVENT_NONE;

    } else if (in == PROTOCOL_PARSING_FAILURE) {

        return AGENT_EVENT_MESSAGE_PARSING_FAILED;
    } else {

        switch (in) {

        case PROTOCOL_PARSED_COO_MESSAGE:
            return AGENT_EVENT_RECEIVED_COO_MESSAGE;
            break;

        case PROTOCOL_PARSED_HIT_MESSAGE:
            return AGENT_EVENT_RECEIVED_HIT_MESSAGE;
            break;

        case PROTOCOL_PARSED_CHA_MESSAGE:
            return AGENT_EVENT_RECEIVED_CHA_MESSAGE;
            break;

        case PROTOCOL_PARSED_DET_MESSAGE:
            return AGENT_EVENT_RECEIVED_DET_MESSAGE;
            break;

        default:
            return AGENT_EVENT_MESSAGE_PARSING_FAILED;
        }
    }
}