#include "Protocol.h"
#include "BOARD.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>



//generateChecksum by xoring all the bytes in the string, custom made function
static int generateChecksum(char data[])
{

    uint8_t tempVar = 0;
    int lengthString = strlen(data);

    int i = 0;
    for (; i < lengthString; i++) {

        tempVar ^= data[i];
    }

    return tempVar;
}

/**
 * Encodes the coordinate data for a guess into the string `message`. This string must be big
 * enough to contain all of the necessary data. The format is specified in PAYLOAD_TEMPLATE_COO,
 * which is then wrapped within the message as defined by MESSAGE_TEMPLATE. The final length of this
 * message is then returned. There is no failure mode for this function as there is no checking
 * for NULL pointers.
 * @param message The character array used for storing the output. Must be long enough to store the
 *                entire string, see PROTOCOL_MAX_MESSAGE_LEN.
 * @param data The data struct that holds the data to be encoded into `message`.
 * @return The length of the string stored into `message`.
 */
int ProtocolEncodeCooMessage(char *message, const GuessData *data)
{

    char dataInitial[256] = {};

    sprintf(dataInitial, PAYLOAD_TEMPLATE_COO, data->row, data->col);

    sprintf(message, MESSAGE_TEMPLATE, dataInitial, generateChecksum(dataInitial));

    return strlen(message);
}

/**
 * Follows from ProtocolEncodeCooMessage above.
 */
int ProtocolEncodeHitMessage(char *message, const GuessData *data)
{

    char dataInitial[256] = {};

    sprintf(dataInitial, PAYLOAD_TEMPLATE_HIT, data->row, data->col, data->hit);

    sprintf(message, MESSAGE_TEMPLATE, dataInitial, generateChecksum(dataInitial));

    return strlen(message);
}

/**
 * Follows from ProtocolEncodeCooMessage above.
 */
int ProtocolEncodeChaMessage(char *message, const NegotiationData *data)
{

    char dataInitial[256] = {};

    sprintf(dataInitial, PAYLOAD_TEMPLATE_CHA, data->encryptedGuess, data->hash);

    sprintf(message, MESSAGE_TEMPLATE, dataInitial, generateChecksum(dataInitial));

    return strlen(message);
}

/**
 * Follows from ProtocolEncodeCooMessage above.
 */
int ProtocolEncodeDetMessage(char *message, const NegotiationData *data)
{

    char dataInitial[256] = {};

    sprintf(dataInitial, PAYLOAD_TEMPLATE_DET, data->guess, data->encryptionKey);

    sprintf(message, MESSAGE_TEMPLATE, dataInitial, generateChecksum(dataInitial));

    return strlen(message);
}



//Tokenization of string to put in structure
static void parseCoodata(char sentence[], GuessData *gData)
{

    //delimited by comma
    //remember: COO,ROW,COL
    //so throw away the first token which is COO.
    //then save ROW and COL into gData

    char * tempPtr = 0;
    tempPtr = strtok(sentence, ","); //unused first token throw away

    tempPtr = strtok(NULL, ",");
    gData->row = atoi(tempPtr);

    tempPtr = strtok(NULL, ",");
    gData->col = atoi(tempPtr);
}

static void parseHitdata(char sentence[], GuessData *gData)
{

    //remember HIT, ROW, COL, HITSTATUS
    //Throw away first, and assign the rest

    char * tempPtr = 0;
    tempPtr = strtok(sentence, ",");

    tempPtr = strtok(NULL, ",");
    gData->row = atoi(tempPtr);

    tempPtr = strtok(NULL, ",");
    gData->col = atoi(tempPtr);

    tempPtr = strtok(NULL, ",");
    gData->hit = atoi(tempPtr);

}

static void parseChadata(char sentence[], NegotiationData *nData)
{

    //CHA, encryptedguess, hash

    char * tempPtr = 0;
    tempPtr = strtok(sentence, ",");

    tempPtr = strtok(NULL, ",");
    nData->encryptedGuess = atoi(tempPtr);

    tempPtr = strtok(NULL, ",");
    nData->hash = atoi(tempPtr);
}

static void parseDetdata(char sentence[], NegotiationData *nData)
{

    //DET, guess, encryptionKey

    char * tempPtr = 0;
    tempPtr = strtok(sentence, ",");

    tempPtr = strtok(NULL, ",");
    nData->guess = atoi(tempPtr);

    tempPtr = strtok(NULL, ",");
    nData->encryptionKey = atoi(tempPtr);
}


//states for state machine in protocol decode
/**
 * This function decodes a message into either the NegotiationData or GuessData structs depending
 * on what the type of message is. This function receives the message one byte at a time, where the
 * messages are in the format defined by MESSAGE_TEMPLATE, with payloads of the format defined by
 * the PAYLOAD_TEMPLATE_* macros. It returns the type of message that was decoded and also places
 * the decoded data into either the `nData` or `gData` structs depending on what the message held.
 * The onus is on the calling function to make sure the appropriate structs are available (blame the
 * lack of function overloading in C for this ugliness).
 *
 * PROTOCOL_PARSING_FAILURE is returned if there was an error of any kind (though this excludes
 * checking for NULL pointers), while
 * 
 * @param in The next character in the NMEA0183 message to be decoded.
 * @param nData A struct used for storing data if a message is decoded that stores NegotiationData.
 * @param gData A struct used for storing data if a message is decoded that stores GuessData.
 * @return A value from the UnpackageDataEnum enum.
 */
typedef enum {
    WAITING, RECORDING, FIRST_CHECKSUM_HALF, SECOND_CHECKSUM_HALF, NEWLINE
} machineParserState;
static machineParserState currentState = WAITING;
static char sentence[256] = {}; //sentence received. translate this into nData or gData
static int index = 0; //index for sentence

static char chkSum[3] = {}; //two hex letters and third character is null. temp storage for checksum validation
static char comparisonMessageID [4] = {}; //temporary storage area to check if COO, HIT, DET, or CHA is receieved. then use a strcmp with "CHA" "DET"  "HIT", or "COO"

ProtocolParserStatus ProtocolDecode(char in, NegotiationData *nData, GuessData *gData)
{
    if (in == '\0'){    //automatically return upon null character
        
        return  PROTOCOL_WAITING;
    }

    switch (currentState) { //if not null, run state machine and process NMEA string

    case WAITING:
    {

        if (in != '$') {

            currentState = WAITING;
            return PROTOCOL_WAITING;
        } else if (in == '$') {

            index = 0;
            memset(sentence, '\0', 256);
            currentState = RECORDING;
            return PROTOCOL_PARSING_GOOD;
        }
    }
        break;

    case RECORDING: //start building NMEA string
    {

        if (in != '*') {

            sentence[index++] = in;
            currentState = RECORDING;
            return PROTOCOL_PARSING_GOOD;

        } else if (in == '*') {

            currentState = FIRST_CHECKSUM_HALF;
            return PROTOCOL_PARSING_GOOD;
        }
    }
        break;

    case FIRST_CHECKSUM_HALF:   //Once nmea string is successfully sent, the checksum must be built and checked for
    {

        if (((in >= 'A') && (in <= 'F')) || ((in >= 'a') && (in <= 'f')) || ((in >= '0') && (in <= '9'))) {

            //build a string for hex character representation of checksum, build for first character
            chkSum[0] = in;
            currentState = SECOND_CHECKSUM_HALF;
            return PROTOCOL_PARSING_GOOD;
        } else {

            currentState = WAITING;
            return PROTOCOL_PARSING_FAILURE;
        }
    }
        break;

    case SECOND_CHECKSUM_HALF:
    {

        if (((in >= 'A') && (in <= 'F')) || ((in >= 'a') && (in <= 'f')) || ((in >= '0') && (in <= '9'))) {

            //build second character for checksum
            chkSum[1] = in;
            chkSum[2] = 0;  //null termination of checksum hex string
            
            //convert hex string to hex value and check against original checksum function for validation of data
            if (strtol(chkSum, NULL, 16) == generateChecksum(sentence)) {

                sentence[index++] = '\0';   //if data is validated, end string with null termination, and go to nexxt state
                currentState = NEWLINE;

                return PROTOCOL_PARSING_GOOD;
            }
        } else {    //if checksum does not match, then need to throw failure error

            currentState = WAITING;
            return PROTOCOL_PARSING_FAILURE;
        }
    }
        break;

    case NEWLINE:   //tkoenie string and decide if coo, cha, hit, det and fill out structure data
    {

        if (in == '\n') {

            //build comparison str for HIT, COO, CHA, or DET
            //copy first 3 characters of sentence which will be, HIT, COO, CHA, or DET
            comparisonMessageID[0] = sentence[0];
            comparisonMessageID[1] = sentence[1];
            comparisonMessageID[2] = sentence[2];
            comparisonMessageID[3] = '\0';

            if (strcmp(comparisonMessageID, "HIT") == 0) {  //tokenize string for fill out function for corresponding type of messages;

                parseHitdata(sentence, gData);
                currentState = WAITING;
                return PROTOCOL_PARSED_HIT_MESSAGE;

            } else if (strcmp(comparisonMessageID, "COO") == 0) {

                parseCoodata(sentence, gData);
                currentState = WAITING;
                return PROTOCOL_PARSED_COO_MESSAGE;

            } else if (strcmp(comparisonMessageID, "CHA") == 0) {

                parseChadata(sentence, nData);
                currentState = WAITING;
                return PROTOCOL_PARSED_CHA_MESSAGE;

            } else if (strcmp(comparisonMessageID, "DET") == 0) {

                parseDetdata(sentence, nData);
                currentState = WAITING;
                return PROTOCOL_PARSED_DET_MESSAGE;
            }
        } else {

            currentState = WAITING;
            return PROTOCOL_PARSING_FAILURE;
        }
    }
        break;
    }
    return PROTOCOL_PARSING_FAILURE;    //if state machine fails and data is corrupt, no state will be entered and thus need to fail
}

/**
 * This function generates all of the data necessary for the negotiation process used to determine
 * the player that goes first. It relies on the pseudo-random functionality built into the standard
 * library. The output is stored in the passed NegotiationData struct. The negotiation data is
 * generated by creating two random 16-bit numbers, one for the actual guess and another for an
 * encryptionKey used for encrypting the data. The 'encryptedGuess' is generated with an
 * XOR(guess, encryptionKey). The hash is simply an 8-bit value that is the XOR() of all of the
 * bytes making up both the guess and the encryptionKey. There is no checking for NULL pointers
 * within this function.
 * @param data The struct used for both input and output of negotiation data.
 */
void ProtocolGenerateNegotiationData(NegotiationData *data)
{

    //clear all bits to zero
    data->guess = 0;
    data->encryptionKey = 0;
    data->encryptedGuess = 0;
    data->hash = 0;

    data->guess = (uint16_t) (rand()); //65535 max value for 16bit number,  typecast to force 16 bit number
    data->encryptionKey = (uint16_t) rand();

    uint8_t mask = 0;
    
    //procedure for function specification, see header file or comment above function definition
    mask ^= data->guess & (0x00FF);
    mask ^= (data->guess & (0xFF00)) >> 8;

    mask ^= (data->encryptionKey & (0x00FF));
    mask ^= (data->encryptionKey & (0xFF00)) >> 8;

    data->hash = mask;

    data->encryptedGuess = ((data->guess & 0xFFFF) ^ (data->encryptionKey & 0xFFFF));
    
}

/**
 * Validates that the negotiation data within 'data' is correct according to the algorithm given in
 * GenerateNegotitateData(). Used for verifying another agent's supplied negotiation data. There is
 * no checking for NULL pointers within this function. Returns TRUE if the NegotiationData struct
 * is valid or FALSE on failure.
 * @param data A filled NegotiationData struct that will be validated.
 * @return TRUE if the NegotiationData struct is consistent and FALSE otherwise.
 */
uint8_t ProtocolValidateNegotiationData(const NegotiationData *data)
{

    //implementation of procedure described in header file or comment section above function defininition;
    uint8_t mask = 0;

    mask ^= data->guess & (0x00FF);
    mask ^= (data->guess & (0xFF00)) >> 8;

    mask ^= (data->encryptionKey & (0x00FF));
    mask ^= (data->encryptionKey & (0xFF00)) >> 8;


    if (((data->encryptedGuess ^ data->encryptionKey) == data->guess) &&
            (data->hash ^ mask) == 0) {

        return SUCCESS; //SUCCESS
    } else {

        return STANDARD_ERROR; //STANDARD_ERROR
    }

}

/**
 * return function returns a TurnOrder enum type representing which agent has won precedence for going
 * first. The value returned relates to the agent whose data is in the 'myData' variable. The turn
 * ordering algorithm relies on the XOR() of the 'encryptionKey' used by both agents. The least-
 * significant bit of XOR(myData.encryptionKey, oppData.encryptionKey) is checked so that if it's a
 * 1 the player with the largest 'guess' goes first otherwise if it's a 0, the agent with the
 * smallest 'guess' goes first. The return value of TURN_ORDER_START indicates that 'myData' won,
 * TURN_ORDER_DEFER indicates that 'oppData' won, otherwise a tie is indicated with TURN_ORDER_TIE.
 * There is no checking for NULL pointers within this function.
 * @param myData The negotiation data representing the current agent.
 * @param oppData The negotiation data representing the opposing agent.
 * @return A value from the TurnOrdering enum representing which agent should go first.
 */
TurnOrder ProtocolGetTurnOrder(const NegotiationData *myData, const NegotiationData *oppData)
{
    uint32_t tempMask = 0;
    tempMask = (myData->encryptionKey ^ oppData->encryptionKey);
    tempMask = (tempMask & 0x01);

    if ((tempMask & 0x01) == 0x01) { //lsb is one, player with largest guess goes first

        if (myData->guess > oppData->guess) {

            return TURN_ORDER_START;
        } else if (myData->guess < oppData->guess) {

            return TURN_ORDER_DEFER;
        } else {

            return TURN_ORDER_TIE;
        }

    } else {

        if (myData->guess < oppData->guess) {

            return TURN_ORDER_START;
        } else if (myData->guess > oppData->guess) {

            return TURN_ORDER_DEFER;
        } else {

            return TURN_ORDER_TIE;
        }
    }
}

