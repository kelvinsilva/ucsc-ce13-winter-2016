#include "Field.h"
#include <stdio.h>
#define TRUE 1
#define FALSE 0

void FieldInit(Field *f, FieldPosition p)
{ //increment through the array and insert FieldPosition
    int i, j;
    for (i = 0; i < FIELD_ROWS; i++) {
        for (j = 0; j < FIELD_COLS; j++) {
            f->field[i][j] = p;
        }
    }
    //Assign boat lives to struct
    f->smallBoatLives = FIELD_BOAT_LIVES_SMALL;
    f->mediumBoatLives = FIELD_BOAT_LIVES_MEDIUM;
    f->largeBoatLives = FIELD_BOAT_LIVES_LARGE;
    f->hugeBoatLives = FIELD_BOAT_LIVES_HUGE;
}

FieldPosition FieldAt(const Field *f, uint8_t row, uint8_t col)
{
    FieldPosition value = f->field[row][col];
    return value;
}

FieldPosition FieldSetLocation(Field *f, uint8_t row, uint8_t col, FieldPosition p)
{
    FieldPosition value = f->field[row][col];
    f->field[row][col] = p;
    return value;
}

uint8_t FieldAddBoat(Field *f, uint8_t row, uint8_t col, BoatDirection dir, BoatType type)
{

    int boatLength = 0;
    FieldPosition boatPosition = FIELD_POSITION_EMPTY;
    switch (type) {
    case FIELD_BOAT_SMALL:
        boatLength = 3;
        boatPosition = FIELD_POSITION_SMALL_BOAT;
        break;
    case FIELD_BOAT_MEDIUM:
        boatLength = 4;
        boatPosition = FIELD_POSITION_MEDIUM_BOAT;
        break;
    case FIELD_BOAT_LARGE:
        boatLength = 5;
        boatPosition = FIELD_POSITION_LARGE_BOAT;
        break;
    case FIELD_BOAT_HUGE:
        boatLength = 6;
        boatPosition = FIELD_POSITION_HUGE_BOAT;
        break;
    }

    switch (dir) {
        int i = 0;
        int bounds = 0;
    case FIELD_BOAT_DIRECTION_EAST:
        bounds = col;
        //Check to make sure placement is empty and in bounds
        for (i = 0; i < boatLength; i++) {
            if ((f->field[row][col + i] != FIELD_POSITION_EMPTY) || (bounds > (FIELD_COLS - 1))) {
                return FALSE;
            }
            bounds++;
        }
        //if went through without returning false, insert boat
        for (i = 0; i < boatLength; i++) {
            f->field[row][col + i] = boatPosition;
        }
        break;
    case FIELD_BOAT_DIRECTION_SOUTH:
        bounds = row;
        //Check to make sure placement is empty and in bounds
        for (i = 0; i < boatLength; i++) {
            if ((f->field[row + i][col] != FIELD_POSITION_EMPTY) || (bounds > (FIELD_ROWS - 1))) {
                return FALSE;
            }
            bounds++;
        }
        //if went through without returning false, insert boat
        for (i = 0; i < boatLength; i++) {
            f->field[row + i][col] = boatPosition;
        }
        break;
    case FIELD_BOAT_DIRECTION_WEST:
        bounds = col;
        //Check to make sure placement is empty and in bounds
        for (i = 0; i < boatLength; i++) {
            if ((f->field[row][col - i] != FIELD_POSITION_EMPTY) || (bounds < 0)) {
                return FALSE;
            }
            bounds--;
        }
        for (i = 0; i < boatLength; i++) {
            f->field[row][col - i] = boatPosition;
        }
        break;
    case FIELD_BOAT_DIRECTION_NORTH:
        bounds = row;
        //Check to make sure placement is empty and in bounds
        for (i = 0; i < boatLength; i++) {
            if ((f->field[row - i][col] != FIELD_POSITION_EMPTY) || (bounds < 0)) {
                return FALSE;
            }
            bounds--;
        }
        for (i = 0; i < boatLength; i++) {
            f->field[row - i][col] = boatPosition;
        }
        break;
    }
    return TRUE;
}

FieldPosition FieldRegisterEnemyAttack(Field *f, GuessData *gData)
{
    //store value to return prev status
    FieldPosition value = f->field[gData->row][gData->col];
    //switch statement between positions
    switch (value) {
        //Assign hit on field and on gData, reduce lives
    case FIELD_POSITION_SMALL_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        if (f->smallBoatLives > 1) {
            gData->hit = HIT_HIT;
            f->smallBoatLives--;
        } else {
            gData->hit = HIT_SUNK_SMALL_BOAT;
            f->smallBoatLives = 0;
        }
        break;
    case FIELD_POSITION_MEDIUM_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        if (f->mediumBoatLives > 1) {
            gData->hit = HIT_HIT;
            f->mediumBoatLives--;
        } else {
            gData->hit = HIT_SUNK_MEDIUM_BOAT;
            f->mediumBoatLives = 0;
        }
        break;
    case FIELD_POSITION_LARGE_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        if (f->largeBoatLives > 1) {
            gData->hit = HIT_HIT;
            f->largeBoatLives--;
        } else {
            gData->hit = HIT_SUNK_LARGE_BOAT;
            f->largeBoatLives = 0;
        }
        break;
    case FIELD_POSITION_HUGE_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        if (f->hugeBoatLives > 1) {
            gData->hit = HIT_HIT;
            f->hugeBoatLives--;
        } else {
            gData->hit = HIT_SUNK_HUGE_BOAT;
            f->hugeBoatLives = 0;
        }
        break;
    default:
        //if miss put miss at that field location
        f->field[gData->row][gData->col] = FIELD_POSITION_MISS;
        gData->hit = HIT_MISS;
        break;
    }
    return value;
}

FieldPosition FieldUpdateKnowledge(Field *f, const GuessData *gData)
{
    //store value to return prev status
    FieldPosition value = f->field[gData->row][gData->col];
    switch (gData->hit) {
        //for hits update as hits on the field, if sunk make lives equal zero
    case HIT_HIT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        break;
    case HIT_SUNK_HUGE_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        f->hugeBoatLives = 0;
        break;
    case HIT_SUNK_LARGE_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        f->largeBoatLives = 0;
        break;
    case HIT_SUNK_MEDIUM_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        f->mediumBoatLives = 0;
        break;
    case HIT_SUNK_SMALL_BOAT:
        f->field[gData->row][gData->col] = FIELD_POSITION_HIT;
        f->smallBoatLives = 0;
        break;
    case HIT_MISS:
        //miss update field as empty
        f->field[gData->row][gData->col] = FIELD_POSITION_EMPTY;
        break;
    }
    return value;
}

uint8_t FieldGetBoatStates(const Field *f)
{
    uint8_t boatStates = (FIELD_BOAT_STATUS_SMALL | FIELD_BOAT_STATUS_MEDIUM | FIELD_BOAT_STATUS_LARGE | FIELD_BOAT_STATUS_HUGE);
    //turn off bit if dead
    if (f->smallBoatLives <= 0) {
        boatStates =  boatStates & ( !(FIELD_BOAT_STATUS_SMALL) );
    }
    if (f->mediumBoatLives <= 0) {
        boatStates = boatStates & ( !(FIELD_BOAT_STATUS_MEDIUM) );
    }
    if (f->largeBoatLives <= 0) {
        boatStates = boatStates & ( !(FIELD_BOAT_STATUS_LARGE) );
    }
    if (f->hugeBoatLives <= 0) {
        boatStates = boatStates & ( !(FIELD_BOAT_STATUS_HUGE) );
    }
    
    
    return boatStates;
}