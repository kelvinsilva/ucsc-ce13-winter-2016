Kelvin Silva

Summary:

To implement battleboats game on microcontrollers. Plays between 2 pic 32 microcontrollers.

Important aspects:

event driven programming, protocol, message, state machines, nmea

Approach to lab:

Read documentation and lab manual.
Sketch out program structure, event driven programming.
implement and test
pair programming

many problems occurred, bugs still on program

Took effort to implement protocol, field, and artificial agent

Implementing lab:

most functionality implemented a few bugs left

I liked that we got to work on a cool project and learn new things.
It was fun learning new hardware and to begin embedded programming

Lab manual extremely hard to read and inconsistent. Needs major revision.
Has references to vriables, non existant in code base. 

Bad documentation. 

Unreadable lab manual.

Hardest part of lab was figuring out specifications for functions to be written.

Lab was not easy to understand.

Battleboats partner: Juan huerta