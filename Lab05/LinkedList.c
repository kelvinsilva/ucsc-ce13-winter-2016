#include "LinkedList.h"
#include <stdlib.h>
#include "BOARD.h"
#include <stdio.h>

/**
 * This function starts a new linked list. Given an allocated pointer to data it will return a
 * pointer for a malloc()ed ListItem struct. If malloc() fails for any reason, then this function
 * returns NULL otherwise it should return a pointer to this new list item. data can be NULL.
 *
 * @param data The data to be stored in the first ListItem in this new list. Can be any valid 
 *             pointer value.
 * @return A pointer to the malloc()'d ListItem. May be NULL if an error occured.
 */

int customCompare(char* leftHandSide, char* rightHandSide);

ListItem *LinkedListNew(char *data)
{

    ListItem* tempList = malloc(sizeof (ListItem));
    if (tempList != NULL) {

        tempList->previousItem = NULL;
        tempList->nextItem = NULL;
        tempList->data = data;
    } else {

        tempList = NULL;
    }

    return tempList;
}

/**
 * This function will remove a list item from the linked list and free() the memory that the
 * ListItem struct was using. It doesn't, however, free() the data pointer and instead returns it
 * so that the calling code can manage it.  If passed a pointer to NULL, LinkedListRemove() should
 * return NULL to signal an error.
 *
 * @param item The ListItem to remove from the list.
 * @return The data pointer from the removed item. May be NULL.
 */
char *LinkedListRemove(ListItem *item)
{

    char* tempPtr = NULL;
    if (item != NULL) {

        item->previousItem->nextItem = item->nextItem;
        item->nextItem->previousItem = item->previousItem;
        tempPtr = item->data;

        free(item);
    } else {

        tempPtr = NULL; //temp ptr is null here
    }

    return tempPtr;
}

/**
 * This function returns the total size of the linked list. This means that even if it is passed a
 * ListItem that is not at the head of the list, it should still return the total number of
 * ListItems in the list. A NULL argument will result in 0 being returned.
 *
 * @param list An item in the list to be sized.
 * @return The number of ListItems in the list (0 if `list` was NULL).
 */
int LinkedListSize(ListItem *list)
{

    ListItem* tempPtr = list;
    int accumulator = 0;

    //accumulator is count add
    if (tempPtr != NULL) {

        //step 1, from current list item return to head.
        while (tempPtr->previousItem != NULL) {

            tempPtr = tempPtr->previousItem;
        }

        //Once first item has been found, then we must traverse to end of list to obtain size.
        while (tempPtr != NULL) {

            tempPtr = tempPtr->nextItem;
            accumulator++;
        }
    }

    return accumulator;
}

/**
 * This function returns the head of a list given some element in the list. If it is passed NULL,
 * it will just return NULL. If given the head of the list it will just return the pointer to the
 * head anyways for consistency.
 *
 * @param list An element in a list.
 * @return The first element in the list. Or NULL if provided an invalid list.
 */
ListItem *LinkedListGetFirst(ListItem *list)
{

    ListItem* tempPtr = list;

    if (list != NULL) {

        //step 1, from current list item return to head.
        while (tempPtr->previousItem != NULL) {

            tempPtr = tempPtr->previousItem;
        }
    }

    return tempPtr;
}

/**
 * This function allocates a new ListItem containing data and inserts it into the list directly
 * after item. It rearranges the pointers of other elements in the list to make this happen. If
 * passed a NULL item, InsertAfter() should still create a new ListItem, just with no previousItem.
 * It returns NULL if it can't malloc() a new ListItem, otherwise it returns a pointer to the new
 * item. The data parameter is also allowed to be NULL.
 *
 * @param item The ListItem that will be before the newly-created ListItem.
 * @param data The data the new ListItem will point to.
 * @return A pointer to the newly-malloc()'d ListItem.
 */
ListItem *LinkedListCreateAfter(ListItem *item, char *data)
{

    ListItem* newItem = malloc(sizeof (ListItem));
    newItem->nextItem = NULL;
    newItem->previousItem = NULL;
    newItem->data = NULL;

    if (newItem != NULL) {

        newItem->previousItem = item;

        if (item != NULL) { //all operations that need to dereference item;

            newItem->nextItem = item->nextItem;

            if (item->nextItem != NULL) {

                item->nextItem->previousItem = newItem;
            }

            item->nextItem = newItem;
        }

        newItem->data = data;
    }

    return newItem;
}

/**
 * LinkedListSwapData() switches the data pointers of the two provided ListItems. This is most
 * useful when trying to reorder ListItems but when you want to preserve their location. It is used
 * within LinkedListSort() for swapping items, but probably isn't too useful otherwise. This
 * function should return STANDARD_ERROR if either arguments are NULL, otherwise it should return
 * SUCCESS. If one or both of the data pointers are NULL in the given ListItems, it still does
 * perform the swap and returns SUCCESS.
 *
 * @param firstItem One of the items whose data will be swapped.
 * @param secondItem Another item whose data will be swapped.
 * @return SUCCESS if the swap worked or STANDARD_ERROR if it failed.
 */
int LinkedListSwapData(ListItem *firstItem, ListItem *secondItem)
{

    char* tempPtr = firstItem->data;
    int successOrFailure = 0;

    firstItem->data = secondItem->data;
    secondItem->data = tempPtr;

    if (firstItem == NULL || secondItem == NULL || firstItem->data == NULL || secondItem->data == NULL) {

        successOrFailure = STANDARD_ERROR;
    } else {

        successOrFailure = SUCCESS;
    }

    return successOrFailure;
}

/**
 * LinkedListSort() performs a selection sort on list to sort the elements into decending order. It
 * makes no guarantees of the addresses of the list items after sorting, so any ListItem referenced
 * before a call to LinkedListSort() and after may contain different data as only the data pointers
 * for the ListItems in the list are swapped. This function sorts the strings in ascending order
 * first by size (with NULL data pointers counting as 0-length strings) and then alphabetically
 * ascending order. So the list [dog, cat, duck, goat, NULL] will be sorted to [NULL, cat, dog,
 * duck, goat]. LinkedListSort() returns SUCCESS if sorting was possible. If passed a NULL pointer
 * for either argument, it will do nothing and return STANDARD_ERROR.
 *
 * @param list Any element in the list to sort.
 * @return SUCCESS if successful or STANDARD_ERROR is passed NULL pointers.
 */
int LinkedListSort(ListItem *list)
{

    if (list != NULL) {

        int linkedListSize = LinkedListSize(list);
        int encounteredNull = 0;
        ListItem* tempListA = LinkedListGetFirst(list);

        int i = 0;

        for (i = 0; i < linkedListSize - 1; i++) {

            int j = 0;
            ListItem* tempListB = tempListA->nextItem;

            for (j = i + 1; j < linkedListSize; j++) {

                if (customCompare(tempListA->data, tempListB->data)) {

                    LinkedListSwapData(tempListA, tempListB);
                }

                tempListB = tempListB->nextItem;
            }
            tempListA = tempListA->nextItem;
        }


        if (encounteredNull) {

            return STANDARD_ERROR;
        } else {

            return SUCCESS;
        }

    } else {

        return STANDARD_ERROR;
    }
}

/**
 * LinkedListPrint() prints out the complete list to stdout. This function prints out the given
 * list, starting at the head if the provided pointer is not the head of the list, like "[STRING1,
 * STRING2, ... ]" If LinkedListPrint() is called with a NULL list it does nothing, returning
 * STANDARD_ERROR. If passed a valid pointer, prints the list and returns SUCCESS.
 *
 * @param list Any element in the list to print.
 * @return SUCCESS or STANDARD_ERROR if passed NULL pointers.
 */
int LinkedListPrint(ListItem *list)
{

    ListItem* tempPtr = LinkedListGetFirst(list);

    if (tempPtr != NULL) {

        printf("\n[ ");
        while (tempPtr != NULL) {

            if (tempPtr->data != NULL) {

                printf(" %s ", tempPtr->data);
            } else {

                printf(" (null) ");
            }

            tempPtr = tempPtr->nextItem;
        }
        printf(" ]\n");

        return SUCCESS;
    } else {

        return STANDARD_ERROR;
    }
}

//First by string length, then by alphabetical, then NULL comes first.

int customCompare(char* leftHandSide, char* rightHandSide)
{

    int returnValue = 0;
    if (leftHandSide == NULL) {

        returnValue = 0;
    } else if (rightHandSide == NULL) {

        returnValue = 1;

    } else if (strlen(leftHandSide) > strlen(rightHandSide)) {

        returnValue = 1;

    } else if (strlen(rightHandSide) > strlen(leftHandSide)) {

        returnValue = 0;

    } else if (strlen(leftHandSide) == strlen(rightHandSide)) {

        if (strcmp(leftHandSide, rightHandSide) < 0) {

            returnValue = 0;

        } else if (strcmp(leftHandSide, rightHandSide) == 0) {

            returnValue = 0;

        } else if (strcmp(leftHandSide, rightHandSide) > 0) {

            returnValue = 1;
        }
    }

    return returnValue;
}













