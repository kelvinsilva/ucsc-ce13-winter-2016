// **** Include libraries here ****
// Standard libraries

//CMPE13 Support Library
#include "BOARD.h"

// Microchip libraries
#include <xc.h>
#include <plib.h>

// User libraries
#include "Buttons.h"
#include "Leds.h"

// **** Set macros and preprocessor directives ****

// **** Declare any datatypes here ****

// **** Define global, module-level, or external variables here ****

typedef struct {
    uint8_t buttonValue;
    uint8_t event;
} ButtonEvents;

ButtonEvents buttonEvents = {0x00, 0x00};

uint8_t event = 0x00;

// **** Declare function prototypes ****

int main(void)
{
    BOARD_Init();

    // Configure Timer 1 using PBCLK as input. This default period will make the LEDs blink at a
    // pretty reasonable rate to start.
    OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_8, 0xFFFF);

    // Set up the timer interrupt with a priority of 4.
    INTClearFlag(INT_T1);
    INTSetVectorPriority(INT_TIMER_1_VECTOR, INT_PRIORITY_LEVEL_4);
    INTSetVectorSubPriority(INT_TIMER_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    INTEnable(INT_T1, INT_ENABLED);

    /***************************************************************************************************
     * Your code goes in between this comment and the following one with asterisks.
     **************************************************************************************************/
    ButtonsInit();
    LEDS_INIT();

    uint16_t ledSequence[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};

    while (1) {

        if (buttonEvents.event == 0xFF) {

            buttonEvents.event = 0x00;
            if ((SWITCH_STATES() & SWITCH_STATE_SW1) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_1DOWN) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[0] | ledSequence[1]);

                    LEDS_SET(tempState);
                }
            } else {

                if (buttonEvents.buttonValue & BUTTON_EVENT_1UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[0] | ledSequence[1]);

                    LEDS_SET(tempState);
                }

            }


            if ((SWITCH_STATES() & SWITCH_STATE_SW2) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_2DOWN) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[2] | ledSequence[3]);

                    LEDS_SET(tempState);
                }
            } else {

                if (buttonEvents.buttonValue & BUTTON_EVENT_2UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[2] | ledSequence[3]);

                    LEDS_SET(tempState);
                }
            }

            if ((SWITCH_STATES() & SWITCH_STATE_SW3) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_3DOWN) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[4] | ledSequence[5]);

                    LEDS_SET(tempState);
                }

            } else {

                if (buttonEvents.buttonValue & BUTTON_EVENT_3UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[4] | ledSequence[5]);

                    LEDS_SET(tempState);

                }
            }


            if ((SWITCH_STATES() & SWITCH_STATE_SW4) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_4DOWN) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[6] | ledSequence[7]);

                    LEDS_SET(tempState);
                }

            } else {

                if (buttonEvents.buttonValue & BUTTON_EVENT_4UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[6] | ledSequence[7]);

                    LEDS_SET(tempState);
                }
            }

        }

    }


    /***************************************************************************************************
     * Your code goes in between this comment and the preceding one with asterisks
     **************************************************************************************************/

    while (1);
}

/**
 * This is the interrupt for the Timer1 peripheral. It checks for button events and stores them in a
 * module-level variable.
 */
void __ISR(_TIMER_1_VECTOR, IPL4AUTO) Timer1Handler(void)
{
    // Clear the interrupt flag.
    INTClearFlag(INT_T1);

    uint8_t tempVal = ButtonsCheckEvents();

    buttonEvents.buttonValue = tempVal;
    buttonEvents.event = 0xFF;


}