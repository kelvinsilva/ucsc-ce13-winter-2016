// **** Include libraries here ****
// Standard libraries

//CMPE13 Support Library
#include "BOARD.h"

// Microchip libraries
#include <xc.h>
#include <plib.h>

// User libraries
#include "Buttons.h"
#include "Leds.h"
#include "Oled.h"


// **** Set macros and preprocessor directives ****

// **** Declare any datatypes here ****

typedef struct {
    uint8_t buttonValue;
    uint8_t event;

} ButtonEvents;

typedef struct {
    uint8_t event;
    uint8_t value;
} TimerResult;

typedef struct {
    uint8_t event;
    uint16_t value;
} AdcResult;

// **** Define global, module-level, or external variables here ****
ButtonEvents buttonEvents = {0x00, 0x00};
TimerResult timerData = {0x00, 0x00};
AdcResult adcData = {0x00, 0x00};

// **** Declare function prototypes ****

int main(void)
{
    BOARD_Init();

    // Configure Timer 1 using PBCLK as input. This default period will make the LEDs blink at a
    // pretty reasonable rate to start.
    OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_8, 0xFFFF);

    // Set up the timer interrupt with a priority of 4.
    INTClearFlag(INT_T1);
    INTSetVectorPriority(INT_TIMER_1_VECTOR, INT_PRIORITY_LEVEL_4);
    INTSetVectorSubPriority(INT_TIMER_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    INTEnable(INT_T1, INT_ENABLED);



    // Enable interrupts for the ADC
    ConfigIntADC10(ADC_INT_PRI_2 | ADC_INT_SUB_PRI_0 | ADC_INT_ON);

    // Set B2 to an input so AN0 can be used by the ADC.
    TRISBCLR = 1 << 2;

    // Configure and start the ADC
    // Read AN0 as sample a. We don't use alternate sampling, so setting sampleb is pointless.
    SetChanADC10(ADC_CH0_NEG_SAMPLEA_NVREF | ADC_CH0_POS_SAMPLEA_AN2);
    OpenADC10(
            ADC_MODULE_ON | ADC_IDLE_CONTINUE | ADC_FORMAT_INTG16 | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON,
            ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF | ADC_SAMPLES_PER_INT_8 |
            ADC_BUF_16 | ADC_ALT_INPUT_OFF,
            ADC_SAMPLE_TIME_29 | ADC_CONV_CLK_PB | ADC_CONV_CLK_51Tcy2,
            ENABLE_AN2_ANA,
            SKIP_SCAN_ALL);
    EnableADC10();

    /***************************************************************************************************
     * Your code goes in between this comment and the following one with asterisks.
     **************************************************************************************************/

    uint16_t ledSequence[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
    uint16_t buttonLightControl = 0x00;
    int i = 0;

    int direction = 1;
    char displayString[50] = {};

    OledInit();
    sprintf(displayString, "Potentiometer value:\n%u\n%.2f %%", adcData.value, (((float) adcData.value / 1024) * 100.0));
    OledDrawString(displayString);

    OledUpdate();

    ButtonsInit();
    LEDS_INIT();


    while (1) {

        if (buttonEvents.event == 0xFF) {

            buttonEvents.event = 0x00;

            if ((SWITCH_STATES() & SWITCH_STATE_SW1) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_1DOWN) {

                    buttonLightControl ^= (ledSequence[0] | ledSequence[1]);

                    LEDS_SET(buttonLightControl);
                }
            } else {

                /*if (buttonEvents.buttonValue & BUTTON_EVENT_1UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[0] | ledSequence[1]);

                    LEDS_SET(tempState);
                }*/

            }


            if ((SWITCH_STATES() & SWITCH_STATE_SW2) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_2DOWN) {

                    buttonLightControl ^= (ledSequence[2] | ledSequence[3]);

                    LEDS_SET(buttonLightControl);
                }
            } else {

                /*if (buttonEvents.buttonValue & BUTTON_EVENT_2UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[2] | ledSequence[3]);

                    LEDS_SET(tempState);
                }*/
            }

            if ((SWITCH_STATES() & SWITCH_STATE_SW3) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_3DOWN) {

                    buttonLightControl ^= (ledSequence[4] | ledSequence[5]);

                    LEDS_SET(buttonLightControl);
                }

            } else {

                /*if (buttonEvents.buttonValue & BUTTON_EVENT_3UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[4] | ledSequence[5]);

                    LEDS_SET(tempState);

                }*/
            }


            if ((SWITCH_STATES() & SWITCH_STATE_SW4) == 0x00) {

                if (buttonEvents.buttonValue & BUTTON_EVENT_4DOWN) {

                    buttonLightControl ^= (ledSequence[6] | ledSequence[7]);

                    LEDS_SET(buttonLightControl);
                }

            } else {

                /*if (buttonEvents.buttonValue & BUTTON_EVENT_4UP) {

                    uint16_t tempState = LEDS_GET();
                    tempState ^= (ledSequence[6] | ledSequence[7]);

                    LEDS_SET(tempState);
                }*/
            }
        }

        if (timerData.event == 0xFF) {

            if ((i == 8 && direction > 0) || (i == -1 && direction < 0)) {

                direction = direction * -1;

            } else {


                uint8_t tempState = buttonLightControl | (ledSequence[i]);
                LEDS_SET(tempState);

            }

            i = i + direction;
            timerData.event = 0x00;
        }

        if (adcData.event == 0xFF) {

            //clear oled
            OledClear(OLED_COLOR_BLACK);
            OledUpdate();

            //display string
            sprintf(displayString, "Potentiometer value:\n%u\n%.2f %%", adcData.value, (((float) adcData.value / 1024) * 100.0));
            OledDrawString(displayString);
            OledUpdate();

            //Clear event flag
            adcData.event = 0x00;
        }

    }
    /***************************************************************************************************
     * Your code goes in between this comment and the preceding one with asterisks
     **************************************************************************************************/

    while (1);
}

/**
 * This is the interrupt for the Timer1 peripheral. It checks for button events and stores them in a
 * module-level variable. Additionally during each call it increments a counter (the value member of
 * a module-level TimerResult struct). This counter is then checked against the top four bits of the
 * ADC result, and if it's greater, then the event member of a module-level TimerResult struct is
 * set to true and the value member is cleared.
 */
void __ISR(_TIMER_1_VECTOR, IPL4AUTO) Timer1Handler(void)
{
    // Clear the interrupt flag.
    INTClearFlag(INT_T1);

    ++timerData.value;
    uint16_t adcAvg = (ADC1BUF0 + ADC1BUF1 + ADC1BUF2 + ADC1BUF3 + ADC1BUF4 + ADC1BUF5 + ADC1BUF6 + ADC1BUF7) >> 3;

    if (timerData.value > adcAvg / 100) {

        timerData.event = 0xFF;
        timerData.value = 0;
    }



    if (adcAvg != adcData.value) {

        adcData.value = adcAvg;
        adcData.event = 0xFF;
    }
}

/**
 * This is the ISR for the ADC1 peripheral. It has been enabled to run continuously. Reads all 8
 * samples from the ADC, averages them, and stores them in a module-level variable for use in the
 * main event loop.
 */
void __ISR(_ADC_VECTOR, IPL2AUTO) AdcHandler(void)
{
    // Clear the interrupt flag.
    INTClearFlag(INT_AD1);

    uint8_t tempVal = ButtonsCheckEvents();

    buttonEvents.buttonValue = tempVal;
    buttonEvents.event = 0xFF;
}