Kelvin Silva

Summary:

To implement various functions in pic32 peripherals, led, pot, switches, buttons

Important aspects:

event driven programming

Approach to lab:

Read documentation and lab manual.
Sketch out program structure, event driven programming.
implement and test

Not many problems occurred.

Took effort to implement debouncing

Implementing lab:

It ended up meeting all specifications with no problem.

I liked that we got to work on a cool project and learn new things.
It was fun learning new hardware and to begin embedded programming

Lab manual was very hard to read and inconsistent. Needs major revision.


Hardest part of lab was debouncing and reading lab manual for project requirements.

Lab was not easy to understand.

Battleboats partner: Juan huerta