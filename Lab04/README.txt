Kelvin Silva

Summary:

To create a 4 function calculator which processes reverse polish notation style calculations

Important aspects:

Tokenization, error handling

Approach to lab:

Create underlying library (stack) and then create calculator functionality.
Check for errors before processing input string

Not many problems occurred.

Took some effort to learn how strtok function works

Implementing lab:

It ended up meeting all specifications with no problem.

I liked that we got to work on a cool project and learn new things.
I did not like that there were a lot of small errors to be made (off by one, termination etc)

No suggestions to alter lab.

I would like  lab manual to be more concise, it is hard to read through.

Hardest part of lab was error handling.

Lab was easy to understand.