// **** Include libraries here ****
// Standard libraries
#include "stdio.h"

//CMPE13 Support Library
#include "BOARD.h"

// Microchip libraries
#include <xc.h>
#include <plib.h>

// User libraries
#include "Stack.h"


// **** Set macros and preprocessor directives ****
#define SIZE_OF_INPUT_STRING 256
// **** Define global, module-level, or external variables here ****

// **** Declare function prototypes ****


int checkValidString(char inputString[]);
void clearCharArrayToZero(char input[], int size);

/*
 * Function prototype for ProcessBackspaces() - This function should be defined
 * below main after the related comment.
 */
int ProcessBackspaces(char *rpn_sentence);

// If this code is used for testing, rename main to something we can call from our testing code. We
// also skip all processor configuration and initialization code.
#ifndef LAB4_TESTING


int main() {
    BOARD_Init();
#else
int their_main(void)
{
#endif // LAB4_TESTING

    /******************************** Your custom code goes below here *******************************/
    char inputString[SIZE_OF_INPUT_STRING] = {};
    char* token;
        
    struct Stack myStk;
    struct Stack* myStkPtr = &myStk;
    
    int errorAlreadyDefined = 0;
    

    
    printf("\nWelcome to calculator RPN Kelvin Silva");
    
    while (1){
        
        errorAlreadyDefined = 0;
        StackInit(myStkPtr);
        clearCharArrayToZero(inputString, SIZE_OF_INPUT_STRING);
        
        printf("\nInput a line in RPN with operators (+ * / -)");
        printf("\n> ");
        fgets(inputString, 256, stdin);

        inputString[strlen(inputString) - 1 ] = '\0'; //take out newline
        
        if (checkValidString(inputString) == 1){
            //do calculations
            
             token = strtok(inputString, " ");
                while (token != NULL){


                    if ( strlen(token) == 1 && (token[0] == '+' || token[0] == '-' || token[0] == '/' || token[0] == '*') ){
                        
                        float leftOperand = 0.0;
                        float rightOperand = 0.0;
                        float result = 0.0;
                        
                        if (StackPop(myStkPtr, &rightOperand ) == STANDARD_ERROR){
                            
                            printf("\nERROR: Not enough operands before operator.\n");
                            errorAlreadyDefined = 1;
                            break;
                        }
                        
                        if (StackPop(myStkPtr, &leftOperand) == STANDARD_ERROR){
                            
                            printf("\nERROR: Not enough operands before operator.\n"); 
                            errorAlreadyDefined = 1;
                            break;
                        }
                        
                        switch (token[0]){
                            
                            case '+':
                                result = leftOperand + rightOperand;
                                break;
                            case '-':
                                result = leftOperand - rightOperand;
                                break;

                            case '*':
                                result = leftOperand * rightOperand;
                                break;

                            case '/':
                                result = leftOperand / rightOperand;
                                break;

                            default:
                                printf("\nERROR Invalid Operator");
                                errorAlreadyDefined = 1;
                                break;
                        }
                        
                        if (StackPush(myStkPtr, result) == STANDARD_ERROR){
                            
                            printf("\nERROR No more room on stack.\n");
                            errorAlreadyDefined = 1;
                            break; 
                        }
                        
                    }else{

                        float tempNumber = atof(token);
                        if (StackPush(myStkPtr, tempNumber) == STANDARD_ERROR){
                            
                            printf("\nERROR No more room on stack\n");
                            errorAlreadyDefined = 1;
                            break;
                        }
                    }
                    token = strtok(NULL, " ");
                }
             
             float finalResult = 0;
             
             if (errorAlreadyDefined == 0){ //make sure to display only one error to the user
                 
                if (StackPop(myStkPtr, &finalResult) == STANDARD_ERROR){

                    printf("\nERROR No items on stack, Invalid RPN Calculation\n");
                    continue;
                }

                if ( StackGetSize(myStkPtr) > 0){

                    printf("\nERROR: Invalid RPN Calculation: more or less than one item in the stack\n");  
                    continue;
                }else {
                 
                 printf ("\nYour result: %f\n", (double)finalResult);       
                }
             }
             
        }else {
            
            printf("\nInvalid character in RPN string\n");
        }   

    }
    

    /*************************************************************************************************/

    // You can never return from main() in an embedded system (one that lacks an operating system).
    // This will result in the processor restarting, which is almost certainly not what you want!
    while (1);
}
int checkValidString(char inputString[]){
    
    int i = 0;
    for (i = 0; i < strlen(inputString); i++){

        if ( (inputString[i] >= '0' && inputString[i] <= '9') || 
              inputString[i] == '+' || inputString[i] == '-' || 
              inputString[i] == '/' || inputString[i] == '*' || 
              inputString[i] == '.' || inputString[i] == ' ' || inputString[i] == '\0') {

             //do nothing and keep checking
        } else {

            return 0;
        }
    }
    return 1;
}

void clearCharArrayToZero(char input[], int size){
    
    int j = 0;
    for (j = 0; j < size; j++){

        input[j] = 0;
    }
}

/**
 * Extra Credit: Define ProcessBackspaces() here - This function should read through an array of
 * characters and when a backspace character is read it should replace the preceding character with
 * the following character. It should be able to handle multiple repeated backspaces and also
 * strings with more backspaces than characters. It should be able to handle strings that are at
 * least 256 characters in length.
 * @param rpn_sentence The string that will be processed for backspaces. This string is modified in
 *                     place.
 * @return Returns the size of the resultant string in `rpn_sentence`.
 */
int ProcessBackspaces(char *rpn_sentence) {
    return 0;
}



