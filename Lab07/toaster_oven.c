// **** Include libraries here ****
// Standard libraries

//CMPE13 Support Library
#include "BOARD.h"

// Microchip libraries
#include <xc.h>
#include <plib.h>

#include "Oled.h"
#include "Adc.h"
#include "Buttons.h"
#include "Leds.h"
#include "Ascii.h"



// **** Set any macros or preprocessor directives here ****
// Set a macro for resetting the timers, makes the code a little clearer.
#define TIMER_2HZ_RESET() (TMR1 = 0)
#define LONG_PRESS 5
// **** Declare any datatypes here ****

// **** Define any module-level, global, or external variables here ****

enum {
    ON, OFF
}; //Other constants

typedef enum {
    RESET,
    START,
    COUNTDOWN,
    PENDING_RESET,
    PENDING_SELECTOR_CHANGE,
    FINISHED_COLOR_INVERT,
    FINISHED_COLOR_NORMAL

} ovenStates;

typedef enum {
    BAKE,
    BROIL,
    TOAST,
    CURRENT

} cookingMode;

enum {
    NO_SELECTION, TEMP_SELECTED, TIME_SELECTED
};

typedef struct {
    uint32_t displayTimeValue;
    uint32_t cookingTimeLeft; //In Seconds
    uint32_t initialCookTime; //In Seconds
    uint32_t temperature; //In farenheight
    cookingMode currentMode;

    uint32_t buttonPressCount;
    uint32_t selection;

    uint32_t onOrOff;

} ovenInformation;

// Configuration Bit settings

//funcs
void printToOled(char* str);
void getAnimationOfOven(char* destination, ovenInformation* ovInfo);

void setDefaultOvenSettings(ovenInformation* ovInfo, cookingMode cM);
void secondsToStringMinutes(char destination[], uint32_t seconds);

//Timer funcs

typedef struct {
    uint32_t event;
    uint32_t value;
    uint32_t onOrOff;

} Timer;

Timer timerOneCookTimer;
Timer timerTwoLongButtonTimerCounter;
Timer flashTimer;

void initializeTimer(Timer* timerData)
{

    timerData->event = 0x00;
    timerData->value = 0;
    timerData->onOrOff = OFF;
}

void startTimer(Timer* timerData)
{

    timerData->onOrOff = ON;
}

void stopTimer(Timer* timerData)
{

    timerData->onOrOff = OFF;
}

void clearTimerData(Timer* timerData)
{

    timerData->event = 0x00;
    timerData->value = 0;
}

int getTimerTimeInSeconds(Timer* timerData)
{

    return (timerData->value >> 1);
}

////////////////

typedef struct {
    uint8_t buttonValue;
    uint8_t event;

} ButtonEvents;

ButtonEvents buttonEvents = {0x00, 0x00};

void ledProgressBar(ovenInformation* ovInfo);
void flashNumberOfTimes(int timesToFlash);

int main()
{
    BOARD_Init();

    // Configure Timer 1 using PBCLK as input. We configure it using a 1:256 prescalar, so each timer
    // tick is actually at F_PB / 256 Hz, so setting PR1 to F_PB / 256 / 2 yields a 0.5s timer.
    OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_256, BOARD_GetPBClock() / 256 / 2);

    // Set up the timer interrupt with a medium priority of 4.
    INTClearFlag(INT_T1);
    INTSetVectorPriority(INT_TIMER_1_VECTOR, INT_PRIORITY_LEVEL_4);
    INTSetVectorSubPriority(INT_TIMER_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    INTEnable(INT_T1, INT_ENABLED);

    // Configure Timer 2 using PBCLK as input. We configure it using a 1:16 prescalar, so each timer
    // tick is actually at F_PB / 16 Hz, so setting PR2 to F_PB / 16 / 100 yields a .01s timer.
    OpenTimer2(T2_ON | T2_SOURCE_INT | T2_PS_1_16, BOARD_GetPBClock() / 16 / 100);

    // Set up the timer interrupt with a medium priority of 4.
    INTClearFlag(INT_T2);
    INTSetVectorPriority(INT_TIMER_2_VECTOR, INT_PRIORITY_LEVEL_4);
    INTSetVectorSubPriority(INT_TIMER_2_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    INTEnable(INT_T2, INT_ENABLED);

    // Configure Timer 3 using PBCLK as input. We configure it using a 1:256 prescalar, so each timer
    // tick is actually at F_PB / 256 Hz, so setting PR3 to F_PB / 256 / 5 yields a .2s timer.
    OpenTimer3(T3_ON | T3_SOURCE_INT | T3_PS_1_256, BOARD_GetPBClock() / 256 / 5);

    // Set up the timer interrupt with a medium priority of 4.
    INTClearFlag(INT_T3);
    INTSetVectorPriority(INT_TIMER_3_VECTOR, INT_PRIORITY_LEVEL_4);
    INTSetVectorSubPriority(INT_TIMER_3_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    INTEnable(INT_T3, INT_ENABLED);

    /***************************************************************************************************
     * Your code goes in between this comment and the following one with asterisks.
     **************************************************************************************************/

    OledInit();
    initializeTimer(&timerOneCookTimer);
    initializeTimer(&timerTwoLongButtonTimerCounter);
    initializeTimer(&flashTimer);

    AdcInit();
    ButtonsInit();
    LEDS_INIT();

    char dest[1024] = {};
    ovenInformation ovInfo;
    setDefaultOvenSettings(&ovInfo, BAKE);

    ovenStates ovenState = RESET;

    while (1) {

        //do the switch to manipulate ovenInfo
        switch (ovenState) {

        case RESET:
        {

            setDefaultOvenSettings(&ovInfo, CURRENT);
            ovenState = START;
        }
            break;

        case START:
        {

            if (buttonEvents.event == 0xFF) {

                if (buttonEvents.buttonValue == BUTTON_EVENT_4DOWN) {

                    startTimer(&timerOneCookTimer);
                    ovInfo.onOrOff = ON;

                    ovenState = COUNTDOWN;
                }

                if (buttonEvents.buttonValue == BUTTON_EVENT_3DOWN) {

                    startTimer(&timerTwoLongButtonTimerCounter);

                    ovenState = PENDING_SELECTOR_CHANGE;
                }

                buttonEvents.event = 0x00;

            } else if (AdcChanged()) {

                switch (ovInfo.selection) {

                case TEMP_SELECTED:
                {
                    uint16_t tempTemperature = ((AdcRead() & (0b1111111100)) >> 2) + 300;
                    ovInfo.temperature = tempTemperature;
                }
                break;

                case TIME_SELECTED:
                {

                    uint16_t timeTemp = ((AdcRead() & 0b1111111100) >> 2) + 1;
                    ovInfo.initialCookTime = timeTemp;
                    ovInfo.displayTimeValue = ovInfo.initialCookTime;
                    ovInfo.cookingTimeLeft = 0;
                }
                    break;
                }
                ovenState = START;
            }

        }
            break;

        case PENDING_SELECTOR_CHANGE:
        {
            printf(" %d ", timerTwoLongButtonTimerCounter.value);
            if (buttonEvents.event == 0xFF) {

                if (buttonEvents.buttonValue == BUTTON_EVENT_3UP) {

                    if (timerTwoLongButtonTimerCounter.value < LONG_PRESS) { //on btn up, stop timer so can count again

                        //bake broil or toast to change mode

                        cookingMode cmTemp = ((ovInfo.currentMode == BAKE) ? TOAST :
                                ((ovInfo.currentMode == TOAST) ? BROIL :
                                ((ovInfo.currentMode == BROIL) ? BAKE : TOAST)));

                        setDefaultOvenSettings(&ovInfo, cmTemp);
                        stopTimer(&timerTwoLongButtonTimerCounter);
                        clearTimerData(&timerTwoLongButtonTimerCounter);
                        ovenState = START;
                    } else {

                        if (ovInfo.currentMode == BAKE) {

                            if (ovInfo.selection == TEMP_SELECTED) {
                                ovInfo.selection = TIME_SELECTED; //switch selector to its opposite

                            } else if (ovInfo.selection == TIME_SELECTED) {
                                ovInfo.selection = TEMP_SELECTED;
                            }

                        } else if (ovInfo.currentMode == TOAST) { //do nothing if toast

                        } else if (ovInfo.currentMode == BROIL) { //do nothing if broil, selector stays the same

                        }

                        stopTimer(&timerTwoLongButtonTimerCounter);
                        clearTimerData(&timerTwoLongButtonTimerCounter);
                        ovenState = START;
                    }

                }

                buttonEvents.event = 0x00;
            }

        }
            break;

        case COUNTDOWN:
        {

            if ((timerOneCookTimer.onOrOff == ON) && ovInfo.cookingTimeLeft < ovInfo.initialCookTime) {

                ovInfo.cookingTimeLeft = getTimerTimeInSeconds(&timerOneCookTimer);
                ovInfo.displayTimeValue = (ovInfo.initialCookTime - ovInfo.cookingTimeLeft);
                ovenState = COUNTDOWN;
            } else if ((timerOneCookTimer.onOrOff == ON) && (ovInfo.cookingTimeLeft >= ovInfo.initialCookTime)) {

                stopTimer(&timerOneCookTimer);
                clearTimerData(&timerOneCookTimer);
                ovenState = FINISHED_COLOR_NORMAL;
            }

            if (buttonEvents.event == 0xFF) {

                if (buttonEvents.buttonValue == BUTTON_EVENT_4DOWN) {

                    ovenState = PENDING_RESET;
                    startTimer(&timerTwoLongButtonTimerCounter);
                }
                buttonEvents.event = 0x00;
            }
        }
            break;

        case PENDING_RESET:
        {

            if (buttonEvents.event == 0xFF) {

                if (buttonEvents.buttonValue == BUTTON_EVENT_4UP) {

                    stopTimer(&timerTwoLongButtonTimerCounter);
                    clearTimerData(&timerTwoLongButtonTimerCounter);
                    ovenState = COUNTDOWN;
                }

                buttonEvents.event = 0x00;
            }

            if ((timerOneCookTimer.onOrOff == ON) && ovInfo.cookingTimeLeft < ovInfo.initialCookTime) {

                ovInfo.cookingTimeLeft = getTimerTimeInSeconds(&timerOneCookTimer);
                ovInfo.displayTimeValue = (ovInfo.initialCookTime - ovInfo.cookingTimeLeft);
                ovenState = PENDING_RESET;
            } else if ((timerOneCookTimer.onOrOff == ON) && ovInfo.cookingTimeLeft >= ovInfo.initialCookTime) {

                stopTimer(&timerOneCookTimer);
                clearTimerData(&timerOneCookTimer);
                ovenState = FINISHED_COLOR_NORMAL;
            }

            if ((timerTwoLongButtonTimerCounter.value >= LONG_PRESS)) {

                stopTimer(&timerTwoLongButtonTimerCounter);
                clearTimerData(&timerTwoLongButtonTimerCounter);
                stopTimer(&timerOneCookTimer);
                clearTimerData(&timerOneCookTimer);
                ovenState = RESET;
            }

        }
            break;

        case FINISHED_COLOR_NORMAL:
        {

            flashNumberOfTimes(1);
            ovenState = FINISHED_COLOR_INVERT;
        }
        break;

        case FINISHED_COLOR_INVERT:
        {

            flashNumberOfTimes(4);
            ovenState = RESET;
        }
        break;

        default:
        {
            printf("ERROR");
        }
        }

        //Now draw oven to screen. and update led
        ledProgressBar(&ovInfo);
        getAnimationOfOven(dest, &ovInfo);
        printToOled(dest);
    }




    /***************************************************************************************************
     * Your code goes in between this comment and the preceding one with asterisks
     **************************************************************************************************/
    while (1);
}

void __ISR(_TIMER_1_VECTOR, ipl4auto) TimerInterrupt2Hz(void) //For Time
{
    // Clear the interrupt flag.
    IFS0CLR = 1 << 4;

    if (timerOneCookTimer.onOrOff == ON) {
        ++timerOneCookTimer.value;
    }

    ++flashTimer.value;
    flashTimer.event = 0xFF;

}

void __ISR(_TIMER_3_VECTOR, ipl4auto) TimerInterrupt5Hz(void) //To distinguish if long press has occurred
{
    // Clear the interrupt flag.
    IFS0CLR = 1 << 12;

    if (timerTwoLongButtonTimerCounter.onOrOff == ON) {
        ++timerTwoLongButtonTimerCounter.value;
    }
}

void __ISR(_TIMER_2_VECTOR, ipl4auto) TimerInterrupt100Hz(void) //For Button events ONLY
{
    // Clear the interrupt flag.
    IFS0CLR = 1 << 8;

    uint8_t tempVal = ButtonsCheckEvents();

    buttonEvents.buttonValue = tempVal;
    buttonEvents.event = 0xFF;
}

void printToOled(char* str)
{

    OledClear(OLED_COLOR_BLACK);
    OledDrawString(str);
    OledUpdate();
}

void ledProgressBar(ovenInformation* ovInfo)
{

    uint16_t ledSequence[9] = {0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF};


    int ledInterval = ovInfo->initialCookTime / 9;
    int timeLeft = ovInfo->displayTimeValue;
    int addressAt = 0;
    if (ledInterval == 0) { //prevent divide by zero

        ledInterval = 1;
    }
    if ((ovInfo->onOrOff == ON)) {

        addressAt = (timeLeft / ledInterval);

        if (addressAt > 8) {
            addressAt = 8;
        } else if (addressAt < 0) {
            addressAt = 0;
        }

        LEDS_SET(ledSequence[addressAt]);
    } else {
        LEDS_INIT();
    }
}

void flashNumberOfTimes(int timesToFlash)
{

    int semaphore = 0;
    while (timesToFlash > 0) {

        if (flashTimer.event == 0xFF) {

            if (semaphore == 0) {
                //invertOled
                OledSetDisplayInverted();
                OledUpdate();
                --timesToFlash;
                semaphore = 1;
            } else if (semaphore == 1) {

                OledSetDisplayNormal();
                OledUpdate();
                semaphore = 0;
            }
            flashTimer.event = 0x00;
        }
    }
    OledSetDisplayNormal();
    OledUpdate();
}

//*****************************************************************
//Data for getAnimationOfOven --stored in ROM

static char topOfOvenOn[6] = {0x01, 0x01, 0x01, 0x01, 0x01, 0};
static char bottomOfOvenOn[6] = {0x03, 0x03, 0x03, 0x03, 0x03, 0};

static char topOfOvenOff[6] = {0x02, 0x02, 0x02, 0x02, 0x02, 0};
static char bottomOfOvenOff[6] = {0x04, 0x04, 0x04, 0x04, 0x04, 0};

static char middleOfOven[6] = {' ', ' ', ' ', ' ', ' ', 0};
static char middleOfOvenRack[] = "-----";

static char blank[] = "         \0";

static char sideOfOven = '|';

static char modeDisplay[32] = {};
static char timeDisplay[32] = {};
static char tempDisplay[32] = {};

void getAnimationOfOven(char* destination, ovenInformation* ovInfo)
{


    //side of oven, top, size, LineMessage
    char tempTimeString[8] = {};
    secondsToStringMinutes(tempTimeString, ovInfo->displayTimeValue);

    switch (ovInfo->currentMode) {

    case BAKE:
    {
        //set status lines first, then draw everything together
        sprintf(modeDisplay, " Mode: Bake");
        sprintf(timeDisplay, " Time: %s", tempTimeString);
        sprintf(tempDisplay, " Temp: %03d%cF", ovInfo->temperature, 0xF8);


        if (ovInfo->selection == TIME_SELECTED) {

            timeDisplay[0] = '>';

        } else if (ovInfo->selection == TEMP_SELECTED) {

            tempDisplay[0] = '>';
        }

        if (ovInfo->onOrOff == ON) {

            sprintf(destination, "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n", sideOfOven, topOfOvenOn, sideOfOven, modeDisplay,
                    sideOfOven, middleOfOven, sideOfOven, timeDisplay,
                    sideOfOven, middleOfOvenRack, sideOfOven, tempDisplay,
                    sideOfOven, bottomOfOvenOn, sideOfOven, blank);
        } else {

            sprintf(destination, "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n", sideOfOven, topOfOvenOff, sideOfOven, modeDisplay,
                    sideOfOven, middleOfOven, sideOfOven, timeDisplay,
                    sideOfOven, middleOfOvenRack, sideOfOven, tempDisplay,
                    sideOfOven, bottomOfOvenOff, sideOfOven, blank);
        }

    }
    break;

    case BROIL:
    {
        sprintf(modeDisplay, " Mode: Broil");
        sprintf(timeDisplay, " Time: %s", tempTimeString);
        sprintf(tempDisplay, " Temp: %03d%cF", ovInfo->temperature, 0xF8);


        if (ovInfo->selection == TIME_SELECTED) {

            timeDisplay[0] = '>';

        } else if (ovInfo->selection == TEMP_SELECTED) {

            tempDisplay[0] = '>';
        }

        if (ovInfo->onOrOff == ON) {

            sprintf(destination, "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n", sideOfOven, topOfOvenOn, sideOfOven, modeDisplay,
                    sideOfOven, middleOfOven, sideOfOven, timeDisplay,
                    sideOfOven, middleOfOvenRack, sideOfOven, tempDisplay,
                    sideOfOven, bottomOfOvenOff, sideOfOven, blank);
        } else {

            sprintf(destination, "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n", sideOfOven, topOfOvenOff, sideOfOven, modeDisplay,
                    sideOfOven, middleOfOven, sideOfOven, timeDisplay,
                    sideOfOven, middleOfOvenRack, sideOfOven, tempDisplay,
                    sideOfOven, bottomOfOvenOff, sideOfOven, blank);
        }

    }
    break;

    case TOAST:
    {

        sprintf(modeDisplay, " Mode: Toast");
        sprintf(timeDisplay, " Time: %s", tempTimeString);
        //note time display set to blank                  

        if (ovInfo->onOrOff == ON) {

            sprintf(destination, "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n", sideOfOven, topOfOvenOff, sideOfOven, modeDisplay,
                    sideOfOven, middleOfOven, sideOfOven, timeDisplay,
                    sideOfOven, middleOfOvenRack, sideOfOven, blank,
                    sideOfOven, bottomOfOvenOn, sideOfOven, blank);
        } else {

            sprintf(destination, "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n"
                    "%c%s%c  %s\n", sideOfOven, topOfOvenOff, sideOfOven, modeDisplay,
                    sideOfOven, middleOfOven, sideOfOven, timeDisplay,
                    sideOfOven, middleOfOvenRack, sideOfOven, blank,
                    sideOfOven, bottomOfOvenOff, sideOfOven, blank);
        }

    }
    break;

    case CURRENT:
    default:
    {

        //do nothing
    }

    }

}

void setDefaultOvenSettings(ovenInformation* ovInfo, cookingMode cM)
{

    if (cM == BAKE) {

        ovInfo->cookingTimeLeft = 0;
        ovInfo->initialCookTime = 1; //one seconds
        ovInfo->displayTimeValue = 1;
        ovInfo->temperature = 350;
        ovInfo->currentMode = BAKE;
        ovInfo->buttonPressCount = 0;
        ovInfo->selection = TIME_SELECTED;
        ovInfo->onOrOff = OFF;

    } else if (cM == BROIL) {

        ovInfo->cookingTimeLeft = 0;
        ovInfo->initialCookTime = 1; //one seconds
        ovInfo->displayTimeValue = 1;
        ovInfo->temperature = 500;
        ovInfo->currentMode = BROIL;
        ovInfo->buttonPressCount = 0;
        ovInfo->selection = TIME_SELECTED;
        ovInfo->onOrOff = OFF;

    } else if (cM == TOAST) {

        ovInfo->cookingTimeLeft = 0;
        ovInfo->initialCookTime = 1; //one seconds
        ovInfo->displayTimeValue = 1;
        ovInfo->temperature = 0; //No temp in toast
        ovInfo->currentMode = TOAST;
        ovInfo->buttonPressCount = 0;
        ovInfo->selection = TIME_SELECTED;
        ovInfo->onOrOff = OFF;
    } else if (cM == CURRENT) {

        ovInfo->cookingTimeLeft = 0;
        ovInfo->initialCookTime = 1; //one seconds
        ovInfo->displayTimeValue = 1;
        ovInfo->buttonPressCount = 0;
        ovInfo->selection = TIME_SELECTED;
        ovInfo->onOrOff = OFF;
    }
}

void secondsToStringMinutes(char destination[], uint32_t seconds)
{

    uint32_t numberOfMinutes = (seconds / 60);
    uint32_t numberOfSeconds = (seconds - (numberOfMinutes * 60));

    sprintf(destination, "%d:%02d", numberOfMinutes, numberOfSeconds);
}
