#ifndef LEDS_H_
#define LEDS_H_

#include "BOARD.h"
#include <xc.h>


#define LEDS_INIT() do { TRISE = 0x00; LATE = 0x00; } while(0)
#define LEDS_SET(x) do { LATE = x; } while (0)
#define LEDS_GET() LATE

#endif //LEDS_H_