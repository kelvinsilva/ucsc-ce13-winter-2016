
#include "Buttons.h"
/**
 * This function initializes the proper pins such that the buttons 1-4 may be used by modifying
 * the necessary bits in TRISD/TRISF. Only the bits necessary to enable the 1-4 buttons are
 * modified so that this library does not interfere with other libraries.
 */
#define NUMBER_OF_BUTTONS 4

static int initialized = 0;

static int iterator = 0;


uint8_t buttonPrevSet[NUMBER_OF_BUTTONS][BUTTONS_DEBOUNCE_PERIOD];
static int previousButtonsAreEqual(uint8_t minimalizedSet[], uint8_t buttonCode);

void ButtonsInit(void)
{

    TRISD |= 0x00E0;
    TRISF |= 0x0002;
    initialized = 1;

    int i = 0;
    for (i = 0; i < NUMBER_OF_BUTTONS; i++) {

        int j = 0;
        for (j = 0; j < BUTTONS_DEBOUNCE_PERIOD; j++) {

            buttonPrevSet[i][j] = 0x00;
        }
    }

    buttonPrevSet[0][0] = BUTTON_STATES() & BUTTON_STATE_1;
    buttonPrevSet[0][1] = BUTTON_STATES() & BUTTON_STATE_2;
    buttonPrevSet[0][2] = BUTTON_STATES() & BUTTON_STATE_3;
    buttonPrevSet[0][3] = BUTTON_STATES() & BUTTON_STATE_4;
}

/**
 * This function checks the button states and returns any events that have occured since the last
 * call. In the case of the first call to ButtonsCheckEvents() after ButtonsInit(), the function
 * should assume that the buttons start in an off state with value 0. Therefore if no buttons are
 * pressed when ButtonsCheckEvents() is first called, BUTTONS_EVENT_NONE should be returned. The
 * events are listed in the ButtonEventFlags enum at the top of this file. This function should be
 * called repeatedly.
 *
 * This function also performs debouncing of the buttons. Every time ButtonsCheckEvents() is called,
 * all button values are saved, up to the 4th sample in the past, so 4 past samples and the present
 * values. A button event is triggered if the newest 4 samples are the same and different from the
 * oldest sample. For example, if button 1 was originally down, button 1 must appear to be up for 4
 * samples before a BUTTON_EVENT_1UP is triggered. This eliminates button bounce, where the button
 * may quickly oscillate between the ON and OFF state as it's being pressed or released.
 *
 * NOTE: This will not work properly without ButtonsInit() being called beforehand.
 * @return A bitwise-ORing of the constants in the ButtonEventFlags enum or BUTTON_EVENT_NONE if no
 *         event has occurred.
 */


uint8_t ButtonsCheckEvents(void)
{

    uint8_t currentState = 0x00;

    uint8_t currentButton1 = (BUTTON_STATES() & BUTTON_STATE_1);
    uint8_t currentButton2 = (BUTTON_STATES() & BUTTON_STATE_2);
    uint8_t currentButton3 = (BUTTON_STATES() & BUTTON_STATE_3);
    uint8_t currentButton4 = (BUTTON_STATES() & BUTTON_STATE_4);




    if (initialized == 0) {

        return BUTTON_EVENT_NONE;
    } else {

    	/*Ternary operation is to access the second index of array at (iterator - 1), 
    	 *but if iterator is zero then access at (iterator)
    	 */
        if (buttonPrevSet[0][((iterator - 1 < 0) ? 0 : (iterator - 1))] != currentButton1) {

            if (previousButtonsAreEqual(buttonPrevSet[0], BUTTON_STATE_1) == 0) {

                currentState |= BUTTON_EVENT_1DOWN;
            } else if (previousButtonsAreEqual(buttonPrevSet[0], BUTTON_STATE_1) == 1) {

                currentState |= BUTTON_EVENT_1UP;
            }
        }

        if (buttonPrevSet[1][((iterator - 1 < 0) ? 0 : (iterator - 1))] != currentButton2) {

            if (previousButtonsAreEqual(buttonPrevSet[1], BUTTON_STATE_2) == 0) {

                currentState |= BUTTON_EVENT_2DOWN;
            } else if (previousButtonsAreEqual(buttonPrevSet[1], BUTTON_STATE_2) == 1) {

                currentState |= BUTTON_EVENT_2UP;
            }

        }


        if (buttonPrevSet[2][((iterator - 1 < 0) ? 0 : (iterator - 1))] != currentButton3) {

            if (previousButtonsAreEqual(buttonPrevSet[2], BUTTON_STATE_3) == 0) {

                currentState |= BUTTON_EVENT_3DOWN;
            } else if (previousButtonsAreEqual(buttonPrevSet[2], BUTTON_STATE_3) == 1) {

                currentState |= BUTTON_EVENT_3UP;
            }
        }


        if (buttonPrevSet[3][((iterator - 1 < 0) ? 0 : (iterator - 1))] != currentButton4) {

            if (previousButtonsAreEqual(buttonPrevSet[3], BUTTON_STATE_4) == 0) {

                currentState |= BUTTON_EVENT_4DOWN;
            } else if (previousButtonsAreEqual(buttonPrevSet[3], BUTTON_STATE_4) == 1) {

                currentState |= BUTTON_EVENT_4UP;
            }

        }

        buttonPrevSet[0][iterator] = currentButton1;
        buttonPrevSet[1][iterator] = currentButton2;
        buttonPrevSet[2][iterator] = currentButton3;
        buttonPrevSet[3][iterator] = currentButton4;
    }

    iterator = iterator + 1;
    if (iterator > 3) {

        iterator = 0;
    }

    return currentState;
}

//returns down or up on a button based on x previous samples.
//up is 0, down is 1, no event is 2
//deboucing algorithm is here. need 4 successsful samples of up or down bit to trigger event.

static int previousButtonsAreEqual(uint8_t minimalizedSet[], uint8_t buttonCode)
{

    int i = 0;
    int retVal = 0;
    int currentUp = 0;
    int currentDown = 0;

    for (i = 0; i < BUTTONS_DEBOUNCE_PERIOD; i++) {

        if (minimalizedSet[i] & buttonCode) {

            currentDown++;
        } else {

            currentUp++;
        }
    }

    if (currentUp == BUTTONS_DEBOUNCE_PERIOD) {

        retVal = 0;
    } else if (currentDown == BUTTONS_DEBOUNCE_PERIOD) {

        retVal = 1;
    } else {

        retVal = 2; //No press
    }

    return retVal;
}