Kelvin Silva

Summary of lab:

Implement 3x3 matrix calculator functions, such as scalar addition, scalar multiplication, matrix multiplication, matrix adiditon, determinant, inverse, and adjugate.
Also implement unit tests for each calculator function.
Important aspects of the lab were to learn code modularization, ex, put each calculator function into c subroutines, as well as learn the concept of unit testing. Learn usage of for loops and matrix indexing, pass by pointer and reference.\

My aproach to the lab was to read through the lab manual and see the requirements, and then I started by simply adding the code to my project.
I got one basic function done, such as matrix equals as well as its unit test, and just repeated the same workflow to the other functions. Eventually the whole program was written.

Not many major issues had happened. One small anomaly was to see the boundaries of FP_DELTA and if the deviation should be inclusive to FP_DElta or exclusive.(Like if FP_DELTA is same as two results subtracted)
I worked by myself for this lab, and only asked the TA one small question about return values.

I implemented the lab successfully, and had no major problems. I had spent around 15 hours frmo writing, to testing the lab.
This lab was worthwhile since it helped me understand the basic workflow in programming. The longest part was writing the unit tests for each function.

Sometimes I had felt that the lab manual was too long, and contained too many unnecessary information, and that it was hard to find important information regarding implementation or specific do's and don'ts or small things that can hurt our grade in the lab.

