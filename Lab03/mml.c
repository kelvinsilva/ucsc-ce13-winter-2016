// **** Include libraries here ****
// Standard libraries
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "MatrixMath.h"
//CMPE13 Support Library
#include "BOARD.h"


// Microchip libraries
#include <xc.h>
#include <plib.h>

// User libraries

// **** Set macros and preprocessor directives ****

// **** Define global, module-level, or external variables here ****

// **** Declare function prototypes ****
void testHarness();

int testMatrixPrint();
int testMatrixEqual();
int testMatrixAdd();
int testMatrixMultiply();
int testMatrixScalarMultiply();
int testMatrixScalarAdd();
int testMatrixDeterminant();
int testMatrixTrace();
int testMatrixTranspose();
int testMatrixAdjugate();
int testMatrixInverse();

int main() {
    BOARD_Init();

    /******************************************************************************
     * Your code goes in between this comment and the following one with asterisks.
     *****************************************************************************/

    testHarness(); //Test harness code.

    /******************************************************************************
     * Your code goes in between this comment and the preceding one with asterisks
     *****************************************************************************/

    // Returning from main() is bad form in embedded environments. So we sit and spin.
    while (1);
}

void testHarness() {

    //Add points from all tests and divide to obtain percentage.
    //Each test runs function twice, and accumulates 2 or less points.

    printf("Running test of matrix math library\n");


    int passedMatrixEquals = testMatrixEqual();
    printf("\nPASSED (%d of 2) MatrixEquals()", passedMatrixEquals);

    int passedMatrixAdd = testMatrixAdd();
    printf("\nPASSED (%d of 2) MatrixAdd()", passedMatrixAdd);

    int passedMatrixMultiply = testMatrixMultiply();
    printf("\nPASSED (%d of 2) MatrixMultiply()", passedMatrixMultiply);

    int passedMatrixScalarMultiply = testMatrixScalarMultiply();
    printf("\nPASSED (%d of 2) MatrixScalarMultiply()", passedMatrixScalarMultiply);

    int passedMatrixScalarAdd = testMatrixScalarAdd();
    printf("\nPASSED (%d of 2) MatrixScalarAdd", passedMatrixScalarAdd);

    int passedMatrixDeterminant = testMatrixDeterminant();
    printf("\nPASSED (%d of 2) MatrixDeterminant", passedMatrixDeterminant);

    int passedMatrixTrace = testMatrixTrace();
    printf("\nPASSED (%d of 2) MatrixTrace()", passedMatrixTrace);

    int passedMatrixTranspose = testMatrixTranspose();
    printf("\nPASSED (%d of 2) MatrixTranspose()", passedMatrixTranspose);

    int passedMatrixAdjugate = testMatrixAdjugate();
    printf("\nPASSED (%d of 2) MatrixAdjugate()", passedMatrixAdjugate);

    int passedMatrixInverse = testMatrixInverse();
    printf("\nPASSED (%d of 2) MatrixInverse()", passedMatrixInverse);

    double totalPassRate = passedMatrixEquals +
            passedMatrixAdd +
            passedMatrixMultiply +
            passedMatrixScalarMultiply +
            passedMatrixScalarAdd +
            passedMatrixDeterminant +
            passedMatrixTrace +
            passedMatrixTranspose +
            passedMatrixAdjugate +
            passedMatrixInverse;

    totalPassRate /= 20.0; //20 points total. points accumulated over points total gives percentage.

    printf("\n%f out of 10 passed\n------------------------------", (double) totalPassRate*10.0);
    
    testMatrixPrint();

}

int testMatrixPrint() {

    float mat1[3][3] = {
        {1.0, 2.0, 3.0},
        {2.0, 3.0, 4.0},
        {0.0, 6.0, 7.0}
    };

    printf("\n\nArray test contents:\n1.0, 2.0, 3.0\n2.0,3.0,4.0\n0.0, 6.0, 7.0");
    printf("\nExpected output:\n| 1.000000 | 2.000000 | 3.000000 |\n"
            "| 2.000000 | 3.000000 | 4.000000 |\n"
            "| 0.000000 | 6.000000 | 7.000000 |\n\n");
    printf("\nCalling MatrixPrint()...\n");

    MatrixPrint(mat1);

    printf("Perform visual check of terminal to see if contents printed are correct");

    return 1;
}

int testMatrixEqual() {

    float mat1[3][3] = {
        {1.0, 2.0, 3.0},
        {2.0, 2.0001, 4.0},
        {0.0, 6.0, 7.0}
    };
    float mat2[3][3] = {
        {1.0, 2.0, 3.0},
        {2.0, 2.00019, 4.0},
        {0.0, 6.0, 7.0}
    };

    int testsPassed = 0;

    if (MatrixEquals(mat1, mat2)) {

        testsPassed++;
    }
    mat1[1][1] = 0.0; //change to different matrixes
    mat2[1][1] = 1.0;

    if (!MatrixEquals(mat1, mat2)) { //matrixEquals supposed to return false here

        testsPassed++;
    }

    return testsPassed;
}

int testMatrixAdd() {

    int testsPassed = 0;

    float mat1[3][3] = {

        {1.0, 1.0, 1.0},
        {1.0, 3.0, 2.0},
        {5.0, 1.0, 1.0}
    };
    float mat2[3][3] = {

        {1.0, 1.0, 3.0},
        {2.0, 2.2, 4.0},
        {0.0, 6.0, 7.0}
    };

    float result[3][3];

    float expectedResult[3][3] = {

        {2.0, 2.0, 4.0},
        {3.0, 5.2, 6.0},
        {5.0, 7.0, 8.0}
    };

    MatrixAdd(mat1, mat2, result);
    if (MatrixEquals(expectedResult, result)) {

        testsPassed++;
    }

    {
        float mat1[3][3] = {

            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0}
        };
        float mat2[3][3] = {

            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0}
        };


        float expectedResult[3][3] = {

            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0},
            {0.0, 0.0, 0.0}
        };
        MatrixAdd(mat1, mat2, result);
        if (MatrixEquals(expectedResult, result)) {

            testsPassed++;
        }
    }
    return testsPassed;
}

int testMatrixMultiply() {

    int testsPassed = 0;

    float mat1[3][3] = {

        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };
    float mat2[3][3] = {

        {2.0, 3.5, 1.0},
        {0.9, 1.0, 3.0},
        {7.0, -1.0, 5.0}
    };

    float result[3][3];

    float expectedResult[3][3] = {

        {15.05, 10.5, 20.5},
        {-7.65, -1, -1.5},
        {58.38, 0.649999, 21.3}
    };

    MatrixMultiply(mat1, mat2, result);

    if (MatrixEquals(expectedResult, result)) {

        testsPassed++;
    }

    float mat3[3][3] = {

        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };
    float mat4[3][3] = {

        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    float expectedResult2[3][3] = {

        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    MatrixMultiply(mat3, mat4, result);

    if (MatrixEquals(expectedResult2, result)) {

        testsPassed++;
    }
    return testsPassed;
}

int testMatrixScalarMultiply() {

    int testsPassed = 0;

    float mat1[3][3] = {

        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };
    float scalar = 3.0;

    float result[3][3];
    float expectedResult[3][3] = {
        { 6.0, 13.5, 3.0},
        {-3.0, 4.5, -3.0},
        {14.1, -23.4, 24.0}
    };

    MatrixScalarMultiply(scalar, mat1, result);
    if (MatrixEquals(expectedResult, result)) {

        testsPassed++;
    }

    float mat2[3][3] = {

        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };
    scalar = 0.0;

    float expectedResult1[3][3] = {
        {0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0}
    };

    MatrixScalarMultiply(scalar, mat2, result);

    if (MatrixEquals(expectedResult1, result)) {

        testsPassed++;
    }

    return testsPassed;
}

int testMatrixScalarAdd() {

    int testsPassed = 0;

    float mat1[3][3] = {

        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };
    float scalar = 3.0;

    float result[3][3];
    float expectedResult[3][3] = {
        {5.0, 7.5, 4.0},
        {2.0, 4.50, 2.0},
        {7.7, -4.8, 11.0}
    };

    MatrixScalarAdd(scalar, mat1, result);
    if (MatrixEquals(expectedResult, result)) {

        testsPassed++;
    }

    float mat2[3][3] = {

        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };
    scalar = 0.0;

    float expectedResult1[3][3] = {
        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };

    MatrixScalarAdd(scalar, mat2, result);
    if (MatrixEquals(expectedResult1, result)) {

        testsPassed++;
    }

    return testsPassed;
}

int testMatrixDeterminant() {

    int testsPassed = 0;

    float mat1[3][3] = {

        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };

    float expectedResult = 24.0;

    float result = MatrixDeterminant(mat1);

    float emptyOne[3][3] = {};
    float emptyTwo[3][3] = {};

    emptyOne[0][0] = expectedResult;
    emptyTwo[0][0] = result;

    if (MatrixEquals(emptyOne, emptyTwo)) {

        testsPassed++;
    }

    float mat2[3][3] = {

        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    expectedResult = 0;

    result = MatrixDeterminant(mat2);

    emptyOne[0][0] = expectedResult;
    emptyTwo[0][0] = result;

    if (MatrixEquals(emptyOne, emptyTwo)) {

        testsPassed++;
    }
    return testsPassed;
}

int testMatrixTrace() {
    int testsPassed = 0;

    float mat1[3][3] = {

        {2.0, 4.5, 1.0},
        {-1.0, 1.50, -1.0},
        {4.7, -7.8, 8.0}
    };

    float expectedResult = 11.50;
    float result = MatrixTrace(mat1);

    float emptyOne[3][3] = {};
    float emptyTwo[3][3] = {};

    emptyOne[0][0] = expectedResult;
    emptyTwo[0][0] = result;

    if (MatrixEquals(emptyOne, emptyTwo)) {

        testsPassed++;
    }

    float mat2[3][3] = {

        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    expectedResult = 0;
    result = MatrixTrace(mat2);

    emptyOne[0][0] = expectedResult;
    emptyTwo[0][0] = result;

    if (MatrixEquals(emptyOne, emptyTwo)) {

        testsPassed++;
    }
    return testsPassed;
}

int testMatrixTranspose() {

    int testsPassed = 0;

    float mat1[3][3] = {
        {1.0, 2.0, 3.0},
        {4.0, 5.0, 6.0},
        {7.0, 8.0, 9.0}
    };

    float expectedResults[3][3] = {
        {1.0, 4.0, 7.0},
        {2.0, 5.0, 8.0},
        {3.0, 6.0, 9.0}
    };

    float results[3][3] = {};

    MatrixTranspose(mat1, results);
    if (MatrixEquals(results, expectedResults)) {

        testsPassed++;
    }

    float mat2[3][3] = {
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    float expectedResults1[3][3] = {
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    MatrixTranspose(mat2, results);
    if (MatrixEquals(results, expectedResults1)) {

        testsPassed++;
    }

    return testsPassed;
}

int testMatrixAdjugate() {

    int testsPassed = 0;

    float mat1[3][3] = {
        {13, 1, 1},
        {1, 0, 1},
        {1, 12, 1}
    };

    float result[3][3] = {};

    float expectedResult[3][3] = {
        {-12.0, 11, 1},
        {0, 12, -12},
        {12, -155, -1}
    };

    MatrixAdjugate(mat1, result);
    if (MatrixEquals(result, expectedResult)) {

        testsPassed++;
    }

    float mat2[3][3] = {
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    float expectedResult1[3][3] = {
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0}
    };

    MatrixAdjugate(mat2, result);
    if (MatrixEquals(result, expectedResult1)) {

        testsPassed++;
    }

    return testsPassed;
}

int testMatrixInverse() {

    int testsPassed = 0;

    float mat1[3][3] = {
        {13, 1, 1},
        {1, 0, 1},
        {1, 12, 1}
    };

    float result[3][3] = {};

    float expectedResult[3][3] = {
        {0.083333, -0.076389, -0.006944},
        {0.000000, -0.083333, 0.083333},
        {-0.083333, 1.076389, 0.006944}
    };

    MatrixInverse(mat1, result);

    if (MatrixEquals(result, expectedResult)) {

        testsPassed++;
    }

    float mat2[3][3] = {

        {0.083333, -0.076389, -0.006944},
        {0.500000, -0.083333, 0.083333},
        {-0.083333, 1.076389, 0.006944}
    };

    float expectedResult1[3][3] = {

        {8.666861, 0.666642, 0.666679},
        {1.000000, 0.0, 1.000000},
        {-51.001369, 8.000182, -3.000091}
    };

    MatrixInverse(mat2, result);
    if (MatrixEquals(result, expectedResult1)) {

        testsPassed++;
    }
    return testsPassed;
}