#include "MatrixMath.h"

//MatrixMath.c implements all of the functions of MatrixMath.h
/******************************************************************************
 * Matrix - Matrix Operations
 *****************************************************************************/

/**
 * MatrixMultiply performs a matrix-matrix multiplication operation on two 3x3
 * matrices and returns the result in the third argument.
 */
void MatrixMultiply(float mat1[3][3], float mat2[3][3], float result[3][3]) {

    float horiz[3] = {}; //init to zero
    float vertical[3] = {};

    int i = 0;
    for (i = 0; i < 3; i++) {

        int q = 0;
        for (q = 0; q < 3; q++) {
            horiz[q] = mat1[i][q]; //grab horizontal of first
        }

        int j = 0;
        for (j = 0; j < 3; j++) {

            int w = 0;
            for (w = 0; w < 3; w++) {

                vertical[w] = mat2[w][j]; //grab the vertical
            }
            int e = 0;
            for (e = 0; e < 3; e++) {

                result[i][j] = horiz[0] * vertical[0] +
                        horiz[1] * vertical[1] +
                        horiz[2] * vertical[2];
            }
        }
    }
}

/**
 * MatrixAdd performs a matrix addition operation on two 3x3 matrices and 
 * returns the result in the third argument.
 */
void MatrixAdd(float mat1[3][3], float mat2[3][3], float result[3][3]) {

    int i = 0;
    int j = 0;

    for (i = 0; i < 3; i++) {

        for (j = 0; j < 3; j++) {

            result[i][j] = mat1[i][j] + mat2[i][j];

        }
    }
}

/**
 * MatrixEquals checks if the two matrix arguments are equal. The result is
 * 0 if FALSE and 1 if TRUE to follow the standard C conventions of TRUE and
 * FALSE.
 */
int MatrixEquals(float mat1[3][3], float mat2[3][3]) {

    int i = 0;
    int j = 0;
    float deltaTest = 0.0;

    for (i = 0; i < 3; i++) {

        for (j = 0; j < 3; j++) {

            deltaTest = fabs(mat1[i][j] - mat2[i][j]);

            if (deltaTest > FP_DELTA) { //check within delta, but if number is equal to delta, number is not equal.

                return 0; //On inequality return right away
            }
        }
    }
    return 1; //If all iterations pass, matrix is equal
}

/******************************************************************************
 * Matrix - Scalar Operations
 *****************************************************************************/

/**
 * MatrixScalarMultiply performs the multiplication of a matrix by a scalar.
 * The result is returned in the third argument.
 */
void MatrixScalarMultiply(float x, float mat[3][3], float result[3][3]) {

    int i = 0;
    int j = 0;

    for (i = 0; i < 3; i++) {

        for (j = 0; j < 3; j++) {

            result[i][j] = mat[i][j] * x;
        }
    }
}

/**
 * MatrixScalarAdd performs the addition of a matrix by a scalar. The result
 * is returned in the third argument.
 */
void MatrixScalarAdd(float x, float mat[3][3], float result[3][3]) {

    int i = 0;
    int j = 0;

    for (i = 0; i < 3; i++) {

        for (j = 0; j < 3; j++) {

            result[i][j] = mat[i][j] + x;
        }
    }
}

/******************************************************************************
 * Unary Matrix Operations
 *****************************************************************************/

/**
 * MatrixDeterminant calculates the determinant of a matrix and returns the
 * value as a float.
 */
float MatrixDeterminant(float mat[3][3]) {

    float det = (
            (mat[0][0] * mat[1][1] * mat[2][2]) +
            (mat[0][1] * mat[1][2] * mat[2][0]) +
            (mat[0][2] * mat[1][0] * mat[2][1]) -
            (mat[0][0] * mat[1][2] * mat[2][1]) -
            (mat[0][1] * mat[1][0] * mat[2][2]) -
            (mat[0][2] * mat[1][1] * mat[2][0]));


    return det;
}

/**
 * MatrixTrace calculates the trace of a matrix. The result is returned as a
 * float.
 */
float MatrixTrace(float mat[3][3]) {

    float accumulator = 0.0;

    //accumulate all values along diagonal
    int i = 0;
    int j = 0;
    for (i = 0, j = 0; i < 3; i++, j++) {
        accumulator += mat[i][j];

    }
    return accumulator;
}

/**
 * MatrixTranspose calculates the transpose of a matrix and returns the
 * result through the second argument
 */
void MatrixTranspose(float mat[3][3], float result[3][3]) {

    int i = 0;

    //switch rows and columns
    for (i = 0; i < 3; i++) {

        int j = 0;
        for (j = 0; j < 3; j++) {

            result[i][j] = mat[j][i];
        }
    }
}

/**
 * MatrixInverse calculates the inverse of a matrix and returns the
 * result through the second argument.
 */
void MatrixInverse(float mat[3][3], float result[3][3]) {

    float determinantReciprocal = 1.0 / MatrixDeterminant(mat);
    float tempResult[3][3] = {};

    MatrixAdjugate(mat, tempResult);

    MatrixScalarMultiply(determinantReciprocal, tempResult, result); //Answer stored in result
}

/**
 * MatrixPrint sends a 3x3 array to standard output with clean formatting.
 * The formatting does not need to look like the expected output given to you
 * in MatricMath.c but each element of the matrix must be separated and have
 * distinguishable position (more than a single line of output).
 */
void MatrixPrint(float mat[3][3]) {

    int i = 0;
    int j = 0;

    for (i = 0; i < 3; i++) {

        for (j = 0; j < 3; j++) {

            printf("| %f ", (double) mat[i][j]);
        }
        printf("|\n");
    }
}

/******************************************************************************
 * Extra Credit Matrix Operations
 *****************************************************************************/

/**
 * Calculate the adjugate matrix of a 3x3 matrix. This is the transpose of the cofactor matrix.
 * @param mat The input 3x3 matrix.
 * @param result The output 3x3 matrix.
 */
void MatrixAdjugate(float mat[3][3], float result[3][3]) {

    float tempMatrixOfMinors[3][3] = {};
    float tempMatrixCofactor[3][3] = {
        {1.0, -1.0, 1.0},
        {-1.0, 1.0, -1.0},
        {1.0, -1.0, 1.0}
    };
    float tempHolding[4] = {};

    int i = 0; //This quadruple nested loop structure is to obtain the matrix of minors
    for (i = 0; i < 3; i++) {

        int j = 0;
        for (j = 0; j < 3; j++) {

            int e = 0;
            int tempHoldingCounter = 0;
            for (e = 0; e < 3; e++) {

                int r = 0;
                for (r = 0; r < 3; r++) {

                    if (i != e && j != r) { //add all elements not in current row or column

                        tempHolding[tempHoldingCounter] = mat[e][r];
                        tempHoldingCounter++;
                    }
                }
            }
            //determinant of 2x2 matrix. since we use simple 2x2, no need for 2d array
            tempMatrixOfMinors[i][j] = (tempHolding[0] * tempHolding[3]) - (tempHolding[1] * tempHolding[2]);

        }
    }

    //Now we perform a multiplication of cofactor matrix to obtain the correct signage.
    for (i = 0; i < 3; i++) {

        int j = 0;
        for (j = 0; j < 3; j++) {

            tempMatrixCofactor[i][j] = tempMatrixCofactor[i][j] * tempMatrixOfMinors[i][j];
        }
    }

    //Now we must transpose the cofactor matrix to obtain the adjugate.
    MatrixTranspose(tempMatrixCofactor, result); //Note that already stored in result, since passed by pointer
}