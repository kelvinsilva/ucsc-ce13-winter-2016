// **** Include libraries here ****
// Standard libraries
#include <stdio.h>
#include <math.h>

//CMPE13 Support Library
#include "BOARD.h"

// Microchip libraries
#include <xc.h>
#include <plib.h>


// User libraries

// **** Set macros and preprocessor directives ****

// **** Define global, module-level, or external variables here ****

// **** Declare function prototypes ****
double addition(double op1, double op2); //dual operand functions
double subtract(double op1, double op2);
double multiply(double op1, double op2);
double divide(double op1, double op2);
double Average(double op1, double op2);

double AbsoluteValue(double op); //single operand functions
double FahrenheitToCelsius(double op);
double CelsiusToFahrenheit(double op);
double Tangent(double op);
double Round(double operand);

// If this code is used for testing, rename main to something we can call from our testing code.
#ifndef LAB2_TESTING

int main(void)
{
    BOARD_Init();
#else

int their_main(void)
{
#endif // LAB2_TESTING

    /******************************************************************************
     * Your code goes in between this comment and the following one with asterisks.
     *****************************************************************************/

    double firstOperand = 0;
    double secondOperand = 0;

    double result = 0;

    char operatorC = 0;

    char *nameTable[300] = {};
    nameTable['v'] = "Averaged with";
    nameTable['a'] = "Absolute value";
    nameTable['c'] = "Farenheight to celcius";
    nameTable['f'] = "Celcius to farenheight";
    nameTable['t'] = "Tangent";
    nameTable['+'] = "+";
    nameTable['-'] = "-";
    nameTable['*'] = "*";
    nameTable['/'] = "/";
    nameTable['r'] = "Rounding";

    printf("\nWelcome to calculator:");


    while (1) {

        printf("\n"
                "\n+ : add"
                "\n- : subtract"
                "\n* : multiply"
                "\n/ : divide"
                "\nv : average"
                "\na : absolute value"
                "\nc : farenheight to celcius"
                "\nf : celcius to farenheight"
                "\nt : tangent function"
                "\nr : round function"
                "");

        printf("\nEnter mathematical operation desired\n: ");
        scanf("%c", &operatorC);
        fseek(stdin, 0, SEEK_END);

        if (operatorC == '+' || operatorC == '-'
                || operatorC == '*' || operatorC == '/' || operatorC == 'v') {

            printf("\nEnter the first operand\n: ");
            scanf("%f", &firstOperand);
            fseek(stdin, 0, SEEK_END);

            printf("\nEnter the second operand\n: ");
            scanf("%f", &secondOperand);
            fseek(stdin, 0, SEEK_END);

            switch (operatorC) {

            case '+': //addition
                result = addition(firstOperand, secondOperand);
                break;

            case '-': //subtraction
                result = subtract(firstOperand, secondOperand);
                break;

            case '*': //multiplication
                result = multiply(firstOperand, secondOperand);
                break;

            case '/': //division
                result = divide(firstOperand, secondOperand);
                break;

            case 'v': //average function
                result = Average(firstOperand, secondOperand);
                break;

            default:

                break;
            }

            //print result
            printf("Result:  %f %s %f = %f ", firstOperand, nameTable[(int) operatorC], secondOperand, result);

        } else if (operatorC == 'a' || operatorC == 'c'
                || operatorC == 'f' || operatorC == 't' || operatorC == 'r') {

            printf("\nEnter argument\n: ");
            scanf("%f", &firstOperand);
            fseek(stdin, 0, SEEK_END);

            switch (operatorC) {

            case 'a': //absolute value
                result = AbsoluteValue(firstOperand);
                break;

            case 'c': //celcius, treat argument as faren
                result = FahrenheitToCelsius(firstOperand);
                break;

            case 'f': //faren, treat argument as celc
                result = CelsiusToFahrenheit(firstOperand);
                break;

            case 't': //tangent in deg
                result = Tangent(firstOperand);
                break;

            case 'r': //round function.
                result = Round(firstOperand);
                break;

            default:
                continue;
                break;
            }

            printf("The %s of %f is %f", nameTable[(int) operatorC], firstOperand, result);
        }
    }

    /******************************************************************************
     * Your code goes in between this comment and the preceding one with asterisks
     *****************************************************************************/
    while (1);
}

/*Add function*/
double addition(double op1, double op2)
{

    return (op1 + op2);
}

double subtract(double op1, double op2)
{

    return (op1 - op2);
}

double multiply(double op1, double op2)
{

    return (op1 * op2);
}

double divide(double op1, double op2)
{

    return (op1 / op2);
}

/********************************************************************************
 * Define the Absolute Value function here.
 ********************************************************************************/

double AbsoluteValue(double op)
{

    return op < 0 ? op*-1.0 : op;
}

/********************************************************************************
 * Define the Average function here.
 *******************************************************************************/

double Average(double op1, double op2)
{

    return ((op1 + op2) / 2.0);
}

/*********************************************************************************
 * Define the Celsius to Fahrenheit function here.
 ********************************************************************************/

double FahrenheitToCelsius(double op)
{

    return ((5.0 / 9.0) * (op - 32.0));
}

/*********************************************************************************
 * Define the Fahrenheit to Celsius function here.
 ********************************************************************************/


double CelsiusToFahrenheit(double op)
{

    return ( ((9.0 / 5.0) * op) + 32);
}

/*********************************************************************************
 * Define the Tangent function that takes input in degrees (Note: Your tangent 
 * function relies on a call from the tangent function of math.h which uses 
 * radians).
 ********************************************************************************/

double Tangent(double op)
{

    double opRad = (op * (M_PI / 180.0));
    double result = tan(opRad);

    return result;
}

/*********************************************************************************
 * Define the Round function here.
 * In order to receive the extra credit your calculator MUST ALSO CORRECTLY utilize
 * this function.
 ********************************************************************************/
double Round(double operand)
{

    int whole = (int) operand;
    double part = AbsoluteValue(operand) - AbsoluteValue((double) whole);

    if (part >= 0.5) { //round away from zero if 0.5 or greater

        whole = (whole < 0 ? whole - 1 : whole + 1); //negative and positive rounding case
    } else {
        //Do nothing if fractional part is less than 0.5
    }

    return whole;
}

